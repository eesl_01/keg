function [Mmob,Mstore] = Matrices_Transport(dx,dy,dz,h,K,n,Dp,al,at,SUPG)
% Compute the mobility and storage matrices for solute transport in 3-D
% dx (nzel x 1): grid size in x-direction
% dy (nyel x 1): grid size in y-direction
% dz (nzel x 1): grid size in z-direction
% h: hydraulic head nx*ny*nz x 1
% Dp: pore diffusion coefficient
% al: longitudinal dispersivity
% at: transverse dispersivity
% SUPG: flag whether fully consistent SUPG method is applied are simple
%       streamline diffusion in the mobility matrix only
% get size of the domain
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);

nel=nxel*nyel*nzel;

% assemble the overall mobility matrix
ivec=zeros(nel*12,1);
jvec=zeros(nel*12,1);
Amobvec=zeros(nel*12,1);
Astovec=zeros(nel,1);
count=1;
for jj=0:nyel-1
    for ii=0:nxel-1
        for kk=0:nzel-1
            % cell index
            ijk=kk*nxel*nyel+ii*nyel+jj;
            % indices to neighbor cells
            mjk=ijk-nyel;
            imk=ijk-1;
            ijm=ijk-nxel*nyel;
            % hydraulic conductivity
            Khere=K(ijk+1);
            % pore volume
            Astovec(ijk+1)=dx(ii+1)*dy(jj+1)*dz(kk+1)*n;
            % connection to left neighbor
            if ii>0
               Kneigh=K(mjk+1);
               dxhere=dx(ii+1);
               dxneigh=dx(ii);
               Know = Khere*Kneigh*(dxhere+dxneigh)/(Khere*dxneigh+Kneigh*dxhere);
               Q    = (h(mjk+1)-h(ijk+1))*Know*2/(dxhere+dxneigh)*dy(jj+1)*dz(kk+1);
               if Q>0
                  ivec(count:count+1)=[ijk;mjk]+1;
                  jvec(count:count+1)=[mjk;mjk]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               else
                  ivec(count:count+1)=[ijk;mjk]+1;
                  jvec(count:count+1)=[ijk;ijk]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               end
               count=count+2;
            end
            % connection to front neighbor
            if jj>0
               Kneigh=K(imk+1);
               dyhere=dy(jj+1);
               dyneigh=dy(jj);
               Know = Khere*Kneigh*(dyhere+dyneigh)/(Khere*dyneigh+Kneigh*dyhere);
               Q    = (h(imk+1)-h(ijk+1))*Know*2/(dyhere+dyneigh)*dx(ii+1)*dz(kk+1);
               if Q>0
                  ivec(count:count+1)=[ijk;imk]+1;
                  jvec(count:count+1)=[imk;imk]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               else
                  ivec(count:count+1)=[ijk;imk]+1;
                  jvec(count:count+1)=[ijk;ijk]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               end
               count=count+2;
            end
            % connection to bottom neighbor
            if kk>0
               Kneigh=K(ijm+1);
               dzhere=dz(kk+1);
               dzneigh=dz(kk);
               Know = Khere*Kneigh*(dzhere+dzneigh)/(Khere*dzneigh+Kneigh*dzhere);
               Q    = (h(ijm+1)-h(ijk+1))*Know*2/(dzhere+dzneigh)*dx(ii+1)*dy(jj+1);
               if Q>0
                  ivec(count:count+1)=[ijk;ijm]+1;
                  jvec(count:count+1)=[ijm;ijm]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               else
                  ivec(count:count+1)=[ijk;ijm]+1;
                  jvec(count:count+1)=[ijk;ijk]+1;
                  Amobvec(count:count+1)=[ -1;  1]*Q;
               end
               count=count+2;
            end
        end
    end
end
Mmob=sparse(ivec(1:count-1),jvec(1:count-1),Amobvec(1:count-1),nel,nel);
Mstore=spdiags(Astovec,0,nel,nel);
end

