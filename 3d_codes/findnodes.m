function nodes = findnodes(Xnod,Ynod,Znod,xrange,yrange,zrange)
% Find all node numbers belonging into a certain range of x-, y-, and
% z-values
% Xnod, Ynod, Znod: nodal coordinates in 3-D arrays
% xrange, yrange, zrange: 2x1 vectors of bounds
% returns node numbers

% node numbers in 3-D array
[nynod,nxnod,nznod]=size(Xnod);
nodnum=reshape(1:nxnod*nynod*nznod,nynod,nxnod,nznod);

nodes=nodnum(Xnod>=xrange(1) & Xnod<=xrange(2) & ...
             Ynod>=yrange(1) & Ynod<=yrange(2) & ...
             Znod>=zrange(1) & Znod<=zrange(2));
nodes=nodes(:);
end