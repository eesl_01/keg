function plotheads(Xnod,Ynod,Znod,h,hin,hout,nsurf)
% Graphical Output
figure(1)
clf
map=jet(nsurf);
hstep=hout+[1:nsurf]/(nsurf+1)*(hin-hout);
for ii=1:nsurf
    p=patch(isosurface(Xnod,Ynod,Znod,h,hstep(ii)));
    set(p, 'FaceColor', map(ii,:),'EdgeColor','none','facealpha',0.75);
    isonormals(Xnod,Ynod,Znod,h,p);
end
view(3);
camlight; lighting phong;
camlight left; lighting phong;
box on
daspect([1 1 1])
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
xlim([min(Xnod(:)) max(Xnod(:))]);
ylim([min(Ynod(:)) max(Ynod(:))]);
zlim([min(Znod(:)) max(Znod(:))]);
drawnow
end
