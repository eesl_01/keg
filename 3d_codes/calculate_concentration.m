% function [m0,m1,cobs,c] = calculate_concentration(dx, dy, dz, nxel, nyel, nzel,...
%                             nod_obs, nodes_well_in, nodes_well_out,...
%                             M, h, lnk, n, ...
%                             Qin, tracer_mass, dt, nt,...
%                             Xc, Yc,plotflag)
function [varargout] = calculate_concentration(lnK,dx, dy, dz, nxel, nyel, nzel,...
                            nod_obs, nodes_well_in, nodes_well_out,...
                            hin, hout, n, Q, tracer_mass, dt, nt,...
                            Xc, Yc, plotflag,datatypeflag, n_store, store_step)
                                         
nel = nxel*nyel*nzel;
Qin = Q(2); % Injection rate at tracer injection well
nod_well = [nodes_well_in;nodes_well_out];
nod_well_inj=nodes_well_in(2);
% Calculate first head field:
[~,h, M] = calculate_head(lnK,dx,dy,dz,hin,hout,nod_obs,nod_well,Q);

% node numbers in 2-D array
elnum=reshape(1:nxel*nyel*nzel,nyel,nxel,nzel);

% compute nodal load
nodein  = elnum(:,1,:);nodein=nodein(:);
nodeout = elnum(:,nxel,:);nodeout=nodeout(:);

bc_dir=unique([nodein(:);nodeout(:);nodes_well_in(:);nodes_well_out(:)]);
loaddir=M(bc_dir,:)*h(:);

%% Initialize Transport
%%disp([datestr(clock) ': Assemble matrices for transport']);
[Mmob,Mstore] = Matrices_Transport(dx,dy,dz,h,exp(lnK),n,[],[],[],[]);

% Diagonal matrix at outflow boundaries
diag_mat_out=zeros(nel,1);
diag_mat_out(bc_dir)=0.5*(loaddir-abs(loaddir));
mat_out=spdiags(diag_mat_out,0,nel,nel);

Mleft                = Mmob -mat_out;
r0                   = zeros(nel,1);
r0(nod_well_inj)   = M(nod_well_inj,:)*h(:)*tracer_mass;

%% Solve for Temporal Moments by Moment-Generating Equations
%%disp([datestr(clock) ': Solve for zeroth moment']);
%%disp([datestr(clock) ': Solve for zeroth and first moments']);
if contains(datatypeflag,'mom')
    m0=reshape(Mleft\r0,nyel,nxel,nzel);
    r1=Mstore*m0(:);
    m1=reshape(Mleft\r1,nyel,nxel,nzel);
end

if contains(datatypeflag,'conc')
    c_cur = zeros(nel,1);
    c = zeros(nel, n_store);
    t_all = zeros(n_store,1);
    cobs = zeros(length(nod_obs), n_store);
    % Change right-hand side for transient transport
    r0(nod_well_inj)   = M(nod_well_inj,:)*h(:)*tracer_mass/Qin/dt;
    % Change left-hand side matrix
    Mleft                = Mmob -mat_out + Mstore/dt;
    %%disp([datestr(clock) ': Factorize left-hand matrix for transient transport']);
    % [L,U]=ilu(Mleft);
    % full lu factorization
    [L_Mtrans,U_Mtrans,P_Mtrans,Q_Mtrans,R_Mtrans]=lu(Mleft);
    Mright               = Mstore/dt;
elseif contains(datatypeflag,'mom')
    mean_arr_times_all = m1(:)./m0(:);
    mean_arr_times_all(isnan(mean_arr_times_all))=0;
    mean_arr_times_obs = mean_arr_times_all(nod_obs(:));

    varargout{1}=mean_arr_times_obs;
    varargout{2}=mean_arr_times_all;
    varargout{3}=nan;
end

%% Time Loop
if contains(datatypeflag,'conc')
    i_ERT = 1;
    for it=1:nt-1
        t = dt*it;
        
        %disp([datestr(clock) ': Solve for concentration at t = ' num2str(t) ' s']);
        % with full factorization
        %if it == 1
    %      c(:,it)=reshape(Q_Mtrans*(U_Mtrans\(L_Mtrans\(P_Mtrans*(R_Mtrans\(r0+Mright*c(:,1)))))),nyel,nxel,nzel);
            %c_cur=Q_Mtrans*(U_Mtrans\(L_Mtrans\(P_Mtrans*(R_Mtrans\(r0+Mright*c_cur(:))))));
        %else
    %      c(:,it)=reshape(Q_Mtrans*(U_Mtrans\(L_Mtrans\(P_Mtrans*(R_Mtrans\(r0+Mright*c(:,it-1)))))),nyel,nxel,nzel);
            c_cur=Q_Mtrans*(U_Mtrans\(L_Mtrans\(P_Mtrans*(R_Mtrans\(r0+Mright*c_cur(:))))));
        %end
        cobs_cur = c_cur(nod_obs(:));
        % BiCGStab with ILU factorization
    %     c=bicgstab(Mleft,r0+Mright*c(:),10*eps,10*nel,L,U,c(:));
        r0 = zeros(nel,1);
%         disp([datestr(clock) ': Concentration solved']);
        if mod(it,store_step)==0
           
           i_ERT = i_ERT+1;
           t_all(i_ERT)=t;
           cobs(:,i_ERT) = cobs_cur;
           c(:,i_ERT) = c_cur;
           
          
           if plotflag
                figure(3)
                contour(Xc,Yc,reshape(c(:,it),length(dy),length(dx)),50);
                hold on
                plot(Xc(nod_obs),Yc(nod_obs),'kx')
                plot(Xc(nod_well),Yc(nod_well),'ko')
                hold off
                xlim([0 sum(dx)])
                ylim([0 sum(dy)])
                colormap jet
                daspect([1 1 1])
                set(gca,'layer','top')
                colorbar
                title('Concentration')
    %           plotheads(Xc,Yc,a,c,max(c(:)),0,10);
    %           kids=get(gca,'children');
    %           set(kids(3:end),'facealpha',.1)
    %           axis tight
    %           grid on
                drawnow
           end % end plot
        end % ed ifstore
    end
    varargout{1}=c;
    varargout{2}=cobs;
    varargout{3}=t_all;
end % if concentrations
end %end function
