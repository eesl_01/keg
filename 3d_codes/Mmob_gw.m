function [M,Mmod,r] = Mmob_gw(dx,dy,dz,K,hin,hout)
% Set up the stiffness matrix for 3-D groundwater flow
% accounting for Dirichlet boundaries
% dx (nzel x 1): grid size in x-direction
% dy (nyel x 1): grid size in y-direction
% dz (nzel x 1): grid size in z-direction
% hin : head in first plane perpendicular to x-direction
% hout: head in last plane perpendicular to x-direction
%
% M : stiffness matrix
% r : righthand-side vectir due to Dirichlet boundaries

flag_bc=true;
if nargout==1 
   flag_bc=false;
end
if nargin==4 
   flag_bc=false;
   if nargout>1
   warning('Can only compute modified matrix if boundary heads are given')
   end
end
% get size of the domain
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);

nel=nxel*nyel*nzel;

% initialize righthand-side vector
if flag_bc
   r = zeros(nel,1);
end

K=reshape(K,nyel,nxel,nzel);

% assemble the overall mobility matrix
ivec1=zeros(nel*12,1);
jvec1=zeros(nel*12,1);
Avec1=zeros(nel*12,1);
if flag_bc
   ivec2=zeros(nyel*nzel*2,1);
   jvec2=zeros(nyel*nzel*2,1);
   Avec2=zeros(nyel*nzel*2,1);
end

count1=1;
count2=1;
for jj=0:nyel-1
    for ii=0:nxel-1
        for kk=0:nzel-1
            % cell index (starting at 0)
            ijk=kk*nxel*nyel+ii*nyel+jj;
            % indices to neighbor cells
            mjk=ijk-nyel;
            imk=ijk-1;
            ijm=ijk-nxel*nyel;
            % hydraulic conductivity
            Khere=K(ijk+1);  
            % connection to left neighbor
            if ii>0
               Kneigh=K(mjk+1);
               dxhere=dx(ii+1);
               dxneigh=dx(ii);
               Know = Khere*Kneigh*(dxhere+dxneigh)/(Khere*dxneigh+Kneigh*dxhere);
               ivec1(count1:count1+3)=[ijk;ijk;mjk;mjk]+1;
               jvec1(count1:count1+3)=[ijk;mjk;ijk;mjk]+1;
               Avec1(count1:count1+3)=[ 1; -1; -1;  1]*Know*2/(dxhere+dxneigh)*dy(jj+1)*dz(kk+1);
               count1 = count1+4;
            elseif flag_bc
               ivec2(count2)=ijk+1;
               jvec2(count2)=ijk+1;
               Avec2(count2)=Khere*2/dx(1)*dy(jj+1)*dz(kk+1);
               count2=count2+1;
               r(ijk+1) = Khere*2/dx(1)*dy(jj+1)*dz(kk+1)*hin;
            end
            if ii==nxel-1 && flag_bc
               ivec2(count2)=ijk+1;
               jvec2(count2)=ijk+1;
               Avec2(count2)=Khere*2/dx(end)*dy(jj+1)*dz(kk+1);
               count2=count2+1;
               r(ijk+1) = Khere*2/dx(end)*dy(jj+1)*dz(kk+1)*hout;
            end
            
            % connection to front neighbor
            if jj>0
               Kneigh=K(imk+1);
               dyhere=dy(jj+1);
               dyneigh=dy(jj);
               Know = Khere*Kneigh*(dyhere+dyneigh)/(Khere*dyneigh+Kneigh*dyhere);
               ivec1(count1:count1+3)=[ijk;ijk;imk;imk]+1;
               jvec1(count1:count1+3)=[ijk;imk;ijk;imk]+1;
               Avec1(count1:count1+3)=[ 1; -1; -1;  1]*Know*2/(dyhere+dyneigh)*dx(ii+1)*dz(kk+1);
               count1 = count1+4;
            end

            % connection to bottom neighbor
            if kk>0
               Kneigh=K(ijm+1);
               dzhere=dz(kk+1);
               dzneigh=dz(kk);
               Know = Khere*Kneigh*(dzhere+dzneigh)/(Khere*dzneigh+Kneigh*dzhere);
               ivec1(count1:count1+3)=[ijk;ijk;ijm;ijm]+1;
               jvec1(count1:count1+3)=[ijk;ijm;ijk;ijm]+1;
               Avec1(count1:count1+3)=[ 1; -1; -1;  1]*Know*2/(dzhere+dzneigh)*dx(ii+1)*dy(jj+1);
               count1 = count1+4;
            end
            
        end
    end
end
M=sparse(ivec1(1:count1-1),jvec1(1:count1-1),Avec1(1:count1-1),nel,nel);
if flag_bc
   Mmod=sparse([ivec1(1:count1-1);ivec2],[jvec1(1:count1-1);jvec2],...
               [Avec1(1:count1-1);Avec2],nel,nel);
end
end
