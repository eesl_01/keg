function [Mmod,rmod]=gwshortcut(M,r,nodes)
% GWSHORTCUT creates a shortcut boundary condition with the given nodes
% INPUT
% M : original stiffness matrix (nnod x nnod)
% r : original right-hand side matrix (nnod x 1)
% nodes: nodes to be shortcut
% OUTPUT
% Mmod: modified stiffness matrix
% rmod: modified right-hand side vector

%disp('Incorporate Dirichlet boundary conditions')

rmod=r;

rmod(nodes(1))=sum(rmod(nodes));
rmod(nodes(2:end))=0;

% Modify the matrix
M(nodes(1),:)=sum(M(nodes,:),1);
M(:,nodes(1))=sum(M(:,nodes),2);
Mmod=rowcoldel(M,nodes(2:end));
