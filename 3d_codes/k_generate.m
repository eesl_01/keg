function Y = k_generate(dx,dy,dz,lx,ly,lz,Ctype,Kg,sigma2,plotflag)
% Generates 3-D log-conductivity field 
% correlation lengths are given in grid blocks
nx=length(dx);Lx=sum(dx);
ny=length(dy);Ly=sum(dy);
nz=length(dz);Lz=sum(dz);

if nz==1
   twoD=true;
else
   twoD=false;
end

nxorg=nx;
nyorg=ny;
nzorg=nz;

% extend to prevent periodicity
nx = nx + 10*lx;
ny = ny + 10*ly;
if ~twoD
   nz = nz + 10*lz; 
end
% total number of nodes
ntot=nx*ny*nz;

% Grid in Physical Coordinates
[X,Y,Z]=meshgrid(-nx/2:(nx-1)/2,-ny/2:(ny-1)/2,-nz/2:(nz-1)/2);

% scaled distance
H=sqrt((X/lx).^2+(Y/ly).^2+(Z/lz).^2);

% Covariance Matrix of Log-Conductivities
if (Ctype==1)
   RYY=exp(-abs(H))*sigma2;
else
   RYY=exp(-H.^2)*sigma2;
end

% correct to obtain requires variance
RYY=RYY-mean(RYY(:));
RYY=RYY*sigma2/max(RYY(:));

% Fourier Transform (Origin Shifted to Node (1,1))
% Yields Power Spectrum of Log-Conductivity
SYY=fftn(fftshift(RYY))/ntot;
% Remove Imaginary Artifacts
SYY=abs(SYY);SYY(1,1,1)=0;

ran=real(ifftn(sqrt(SYY).*(randn(size(SYY))+1i*randn(size(SYY)))*ntot));
% K=exp(ran)*Kg;
Y = ran/log(10)+log10(Kg);
% Y=ran+log(Kg);
Y=Y(1:nyorg,1:nxorg,1:nzorg);
if plotflag
   figure(1)
   set(gcf,'outerposition',get(0,'screensize'))
   [Xex,Yex,Zex]=meshgrid(cumsum([0 dx])-sum(dx)*.5,...
                          cumsum([0 dy])-sum(dy)*.5,...
                          cumsum([0 dz])-sum(dz));
   ranex=zeros(size(Xex));
   ranex(1:ny,1:nx,1:nz)=ran;
   ranex=ranex/log(10)+log10(Kg);
   
   ranex = ranex(1:nyorg+1,1:nxorg+1,1:nzorg+1);   
   slice(Xex,Yex,Zex,ranex,[-.25 .25]*Lx,[],[-.75 -.25]*Lz);
   hold on
   plot3(-.25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
   plot3( .25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
   plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.25*[1 1 1 1 1]*Lz,'k')
   plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.75*[1 1 1 1 1]*Lz,'k')
   plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
   plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
   plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
   plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
   hold off
   clear Xex Yex Zex ranex
   daspect([1 1 1]);
   colormap jet
   box on;
   set(gca,'xgrid','on','ygrid','on','zgrid','on','fontsize',12);
   set(gca,'clim',sqrt(sigma2)/log(10)*3*[-1 1]+log10(Kg));
   xlabel('x_1');ylabel('x_2');zlabel('x_3');
   axis tight
   shading flat;
   title('log-conductivity field')
   colorbar
   set(gcf,'paperunits','centimeters','paperposition',[1 1 20 10])
   drawnow;
end

end
