function nodes_all = findnodesmany(Xnod,Ynod,Znod,xrange,yrange,zrange)
% Find all node numbers belonging into a certain range of x-, y-, and
% z-values
% Xnod, Ynod, Znod: nodal coordinates in 3-D arrays
% xrange, yrange, zrange: nx2 vectors of bounds, with n the number of 
% searches to do. 
% returns node numbers
if ~isequal(size(xrange),size(yrange),size(zrange))
   disp([datestr(clock) ': Dimenison issues when searching for well nodes']);
   return
end

% node numbers in 3-D array
[nynod,nxnod,nznod]=size(Xnod);
nodnum=reshape(1:nxnod*nynod*nznod,nynod,nxnod,nznod);

[nwells,~]=size(xrange);
nodes_all=struct;
for cur_well=1:nwells
    
   nodes=nodnum(Xnod>=xrange(cur_well,1) & Xnod<=xrange(cur_well,2) & ...
                Ynod>=yrange(cur_well,1) & Ynod<=yrange(cur_well,2) & ...
                Znod>=zrange(cur_well,1) & Znod<=zrange(cur_well,2));

   well_no = strcat('well_',num2str(cur_well));
   nodes_all.(well_no)=nodes(:);
end
if length(fieldnames(nodes_all)) == 1
    nodes_all = nodes_all.(well_no);
end
end

