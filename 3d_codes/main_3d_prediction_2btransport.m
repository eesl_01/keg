clear
close all
%% Do DA with a 3D flow and transport model
% Generates a 3D reference scenario
% Runs either steady-state flow alone or coupled with transport
% THIS VERSION IS FOR THE ASSIMILATION OF REAL HT AND TT DATA
% Make an initial run to evaluate and plot the initial state of the model?
make_inirun = false;
% Generate initial ensemble?
gen_ensemble = false;
% Do the parameter updating? Activate inversion
run_inversion = true;
% Assimilate hydraulic head data? (run flow model?)
inversion_heads = false;
% Assimilate tracer data? (run transport model?)
inversion_conc = true;
% Type of data to assimilate (only transport): moments or concentration
datatypeflag = 'moments';
% Limit parameter updating to the inner model area
invert_inner_zone = true;
% If not first assimilated test, set to false and give source file name
first_assimtest = false;
initial_ensemble_dir = '3d_test_3bTR_results_transport_512mem_nodamp_03sigmah.mat';

%% additional settings
% Number of realizations
nreal = 128*4;
% Test name:
test_name = '3d_test_2b';
% Additional test identifier:
addstr = 'prediction';
% Store data?
storeData = true;
% Activate plot settings?
plotflag = false;
% Run predicitions at the end of the assimialtion?
run_predictions = false;

%% Set path to AMG preconditioner and check whether the mex-file can be found
addpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
javaaddpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
addpath('/home/olaf/hsl_mi20-2.0.0/matlab/')
javaaddpath('/home/olaf/hsl_mi20-2.0.0/matlab/')

if exist('hsl_mi20','file')==3
   AMGflag = true;
   disp([datestr(clock) ': AMG preconditioner found']);
else
   AMGflag = false;
   disp([datestr(clock) ': No AMG preconditioner found, work with ILU']);
end

%% reset the global random number stream based on current time
s = RandStream('mt19937ar','Seed','shuffle');
RandStream.setGlobalStream(s);

%% Define the grid
zflotramin = -10.01;%-9.01;
zflotramax = -1.99; %-2.99;
disp([datestr(now) ': Creating model grid'])
% build the grid: coordinates of nodes
[dx,dy,dz,Xc,Yc,Zc,dzflotra,Xflotra,Yflotra,Zflotra]=...
    grid3D(zflotramin,zflotramax);
% number of elements in all directions
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);
nzelflotra=length(dzflotra);
nel=nxel*nyel*nzel;
nelflotra=nxel*nyel*nzelflotra;

% mapper indices large domain -> flow-and-transport domain
z_map_flotra = findnodes(Xc(1,1,:),Yc(1,1,:),Zc(1,1,:),...
                         Xc(1)+[-.01 0.01],Yc(1)+[-0.01 0.01],...
                         [zflotramin zflotramax]);
% node numbers in 3-D array
elnum=reshape(1:nxel*nyel*nzel,nyel,nxel,nzel);
elnumflotra=reshape(1:nxel*nyel*nzelflotra,nyel,nxel,nzelflotra);

% cells at the in- and outflow faces
nodein  = elnum(:,1,:);nodein=nodein(:);
nodeout = elnum(:,nxel,:);nodeout=nodeout(:);
nodeinflotra  = elnumflotra(:,1,:);nodeinflotra=nodeinflotra(:);
nodeoutflotra = elnumflotra(:,nxel,:);nodeoutflotra=nodeoutflotra(:);

%% Define observation points and pumping wells
% Define the wells            
% well_1: outer injection well          well_4: inner injection well bottom
% well_2: inner injection well top      well_5: inner extraction well   
% well_3: inner injection well middle   well_6: outer extraction well     
% Injection and extraction rates [m3/s]
% Test 2b:
wellObj.Q = struct('Qin1', 4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.81e-3,...
    'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -9e-3);

% Test 1a: 
% wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.6e-3, 'Qin3', 0.8e-3,...
%    'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -9e-3);

% Test 3b: 
%wellObj.Q = struct('Qin1', 4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.82e-3,...
%   'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -8.7e-3);
% Test 3a: 22062016
%wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.83e-3, 'Qin3', 0.9e-3,...
%                   'Qin4', 0.6e-3, 'Qout1', -2.5e-3, 'Qout2', -9.0e-3);
% Test 1b: 
% wellObj.Q = struct('Qin1',4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.87e-3,...
%                    'Qin4', 0.6e-3, 'Qout1', -2.4e-3, 'Qout2', -9.0e-3);

% nodes belonging to the injection and extraction screens
rangeXwells = [-18.01 -17.99; -8.01  -7.99; -8.01  -7.99; -8.01  -7.99; 7.99  8.01; 12.99 13.01];
rangeYwells = [-0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01];
rangeZwells = [-9.01 -2.99; -4.51 -3.49;  -6.51 -5.49; -8.51 -7.99; -9.01 -2.99; -9.01 -2.99];
wellObj.nod_well = findnodes_many(Xflotra,Yflotra,Zflotra,...
                           rangeXwells,rangeYwells,rangeZwells);
% nodes belonging to the observation wells
% 1 -- 6 -- 11 -- 16
% |    |     |    |             xflat = Xflotra(:,:,1);
% 2 -- 7 -- 12 -- 17            yflat= Yflotra(:,:,1);
% |    |     |    |             plot(xflat(:), yflat(:), '.')
% 3 -- 8 -- 13 -- 18           
% |    |     |    |
% 4 -- 9 -- 14 -- 19
% |    |     |    |
% 5 -- 10 -- 15 -- 20
% rangeXwells_obs = [-5.01 -4.99;  -5.01 -4.99; -5.01 -4.99; -5.01 -4.99; -5.01 -4.99];
% rangeYwells_obs = [4.99 5.01;    1.6     1.7;  -0.01 0.01; -1.7 -1.6;  -5.01 -4.99];
% rangeZwells_obs = [-10.01 -1.99; -2.3 -2.1;  -10.01 -1.99; -10.01 -1.99;  -10.01 -1.99];
ob_well_file = './test_wells2b_TRANSP.txt';
dwdn_obs_file = './test_2b_29062016_arrivaltimes.txt';
% arriv_obs_file = '';

% load observation well ranges from file
fid = fopen(ob_well_file);
data = textscan(fid, '%f %f %f %f %f %f%*[^\n]','HeaderLines',1);
fid = fclose(fid);

rangeXwells_obs = [data{1} data{2}];
rangeYwells_obs = [data{3} data{4}];
rangeZwells_obs = [data{5} data{6}];

wellObj.nod_obs = findnodes_many(Xflotra,Yflotra,Zflotra,...
                            rangeXwells_obs, rangeYwells_obs, rangeZwells_obs);
                            
wellObj.nod_obs_c = wellObj.nod_obs;
% number of observations
fields = fieldnames(wellObj.nod_obs);
nobs = numel(fields);            
if invert_inner_zone
    inner_zone_nodes = findnodes_many(Xflotra,Yflotra,Zflotra, [-19.26, 15.26], [-9.26, 9.26],[-9.01 -2.99]);
end

%% Load real data:
fid = fopen(dwdn_obs_file);
s_obs_ref = textscan(fid, '%f%*[^\n]','HeaderLines',1);
fid = fclose(fid);
s_obs_ref = s_obs_ref{1};
% uncertainty of heads
sigma_h = 0.03; %0.01;

%% Boundary conditions
hin  = 0.01; %0.33;
hout = 0;

%% Define transport parameters and tracer injection
% porosity
n = 0.08;            %0.3; %0.08;
% injected mass of tracer [kg]
tracer_mass = 1.1e-3;
% which screen is the injection screen ('top','middle','bottom')
which_in='middle';

%% Time discretization
dt = 60; %60;          % time increment [s]
tend = 86400; %86400;     % end time [s]
nt = tend/dt+1;   % number of time steps
store_step = 20;  % how often is c stored (every store_step-th time)
n_store=(nt-1)/store_step+1; % number of stored concentrations
%dt_store = dt*store_step;

nobs=length(s_obs_ref);
load(initial_ensemble_dir, 'lnKpost_c');
[nel,nreal_post] = size(lnKpost_c);
heads_pred=nan(nel,nreal_post);
dwdn_pred=nan(nobs,nreal_post);

parfor ireal=1:nreal_post
       %call_pred(:,:,ireal)
   [heads_pred(:,ireal),~,~, dwdn_pred(:,ireal), ~] = calculate_head3d(lnKpost_c(:,ireal),...
                                                 nyel, nxel, nzelflotra,nelflotra,...
                                                 dx,dy,dzflotra,...
                                                 Xflotra,Yflotra,Zflotra,...
                                                 hin, hout,wellObj,... 
                                                 AMGflag, plotflag);
end
poolobj = gcp('nocreate');
delete(pool)



% parfor ireal=1:nreal_post
%    %call_pred(:,:,ireal)
%    [cobs_ini(:,:,ireal), ~, ~,~,~] = calculate_concentration3d(lnKpost_c(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
%                                     nelflotra, nyel,nxel,nzelflotra,hin, hout,...
%                                     Xflotra,Yflotra,Zflotra,...
%                                     wellObj, n,tracer_mass,...
%                                     dt, nt, store_step, n_store, which_in,...
%                                     plotflag, AMGflag, 'conc');
% end
%    
% 
%     
%     if run_predictions
%         % Run first flow:
%         heads_pred = nan(nobs,n_store,nreal_post);
%         heads_ini = nan(nobs,n_store,nreal);
%         dwdn_pred= nan(nobs,n_store,nreal_post);
%         dwdn_ini= nan(nobs,n_store,nreal);
%         parfor ireal=1:nreal
% %             [heads_pred,Yall,~, dwdn_pred, sall]
%             [heads_pred(:,ireal),~,~, dwdn_pred(:,ireal), ~] = calculate_head3d(lnKpost(:,ireal),...
%                                                  nyel, nxel, nzelflotra,nelflotra,...
%                                                  dx,dy,dzflotra,...
%                                                  Xflotra,Yflotra,Zflotra,...
%                                                  hin, hout,wellObj,... 
%                                                  AMGflag, plotflag);
%         end
%         parfor ireal=1:nreal_post
% %             [heads_pred,Yall,~, dwdn_pred, sall]
%             [heads_ini(:,ireal),~,~, dwdn_ini(:,ireal), ~] = calculate_head3d(lnKpost(:,ireal),...
%                                                  nyel, nxel, nzelflotra,nelflotra,...
%                                                  dx,dy,dzflotra,...
%                                                  Xflotra,Yflotra,Zflotra,...
%                                                  hin, hout,wellObj,... 
%                                                  AMGflag, plotflag);
%         end
%         save(strcat(test_name,'_results_heads_PREDICTION',addstr), ...
%                                                     'heads_pred', 'heads_ini',...
%                                                     'dwdn_pred','dwdn_ini', '-v7.3')
%     end
%     meanlnKpost = mean(lnKpost,2);
%     meanhall    = mean(hall,2);
%     stdlnKpost  = std(lnKpost,[],2);
%     
%     if info.plotflag
%         nx=length(dx);Lx=sum(dx);
%         ny=length(dy);Ly=sum(dy);
%         nz=length(dzflotra);Lz=sum(dzflotra);
%         lnKr = reshape(meanlnKpost, length(dy),length(dx), length(dzflotra));
%         
%         figure(1)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(2,2,1)
%         h = slice(Xflotra,Yflotra,Zflotra,lnKr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('True Field of ln(K)')
%         ca_lnKtrue=caxis;
%         hold on
%         tempobs = fieldnames(wellObj.nod_obs);
%         tempwell =   fieldnames(wellObj.nod_well);
%         
%         for cur_field=1:numel(tempwell)
%             cur_well_nodes = wellObj.nod_well.(tempwell{cur_field});
%             scatter3(Xflotra(cur_well_nodes), Yflotra(cur_well_nodes), Zflotra(cur_well_nodes),7, 'k', 'filled');
%         end         
%         
%         
%         for cur_field=1:numel(tempobs)
%             cur_obs_nodes = wellObj.nod_obs.(tempobs{cur_field});
%             scatter3(Xflotra(cur_obs_nodes), Yflotra(cur_obs_nodes), Zflotra(cur_obs_nodes),5, 'k', 'filled');
%         end         
%         hold off
%         
%         meanlnKpriorr = reshape(meanlnKprior, length(dy),length(dx), length(dzflotra));
%         figure(1)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(2,2,2)
%         h1 = slice(Xflotra,Yflotra,Zflotra,meanlnKpriorr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h1,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Prior Mean of ln(K)')
%         caxis(ca_lnKtrue);
%         
%         meanlnKpostr = reshape(meanlnKpost, length(dy),length(dx), length(dzflotra));
%         figure(1)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(2,2,3)
%         h2 = slice(Xflotra,Yflotra,Zflotra,meanlnKpostr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h2,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Posterior Mean of ln(K)')
%         caxis(ca_lnKtrue);
% 
%         
%         stdlnKriorr= reshape(stdlnKprior, length(dy),length(dx), length(dzflotra));
%         figure(5)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(1,3,1)
%         h3 = slice(Xflotra,Yflotra,Zflotra,stdlnKriorr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h3,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Prior Standard Deviation of ln(K)')
%         ca_stdprior=caxis;
%         
%         stdlnKpostr= reshape(stdlnKpost, length(dy),length(dx), length(dzflotra));
%         figure(5)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(1,3,2)
%         h4 = slice(Xflotra,Yflotra,Zflotra,stdlnKpostr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h4,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Posterior Standard Deviation of ln(K): flow')
%         caxis(ca_stdprior);
%         
%         figure(3)
%         normres=(h_sim(:)-reshape(data_h*ones(1,size(h_sim,2)),nobs*size(h_sim,2),1))/sigma_h;
%         handle=histfit(normres);
%         set(handle(1),'facecolor',[.5 .5 .5],'edgecolor','none')
%         xlabel('(h_{sim}-h_{meas})/\sigma_h [-]')
%         ylabel('count')
%         set(handle(2),'color','k')
%         [fitmean,fitstd]=normfit(normres);
%         legend('final ensemble',sprintf('N(%5.2f,%5.2f)',[fitmean,fitstd]))
%     end
%     if inversion_conc
%         disp('Assimilate concentration data')
%         %% uncertainty of concentrations
%         %sigma_c = 1e5;
%         info.transport = false;
%         % add measurement error to obtain virtual measurements
%         sigma_c=0.25.*arr_timesobs_ref;
%         %arr_timesobs(arr_timesobs==0) = 1e10;
%         data_c = arr_timesobs_ref+randn(length(arr_timesobs_ref),1).*sigma_c;
%         sigma_c = sigma_c(~isnan(data_c)&data_c>0);
%         temp = fieldnames(wellObj.nod_obs_c);
%         temp = temp(~isnan(data_c)&data_c>0);
%         
%         data_c = data_c(~isnan(data_c)&data_c>0);
%         
%         for cur_field=1:numel(temp)
%             temp2.nod_obs_c.(temp{cur_field}) = wellObj.nod_obs_c.(temp{cur_field});
%         end
%         wellObj.nod_obs_c= temp2.nod_obs_c;
%         
%         nobs_c=length(data_c);
%         % Cdd_c = spdiags(sigma_c^2*ones(nobs_c,1),0,nobs_c,nobs_c);
%         %sigma_c(sigma_c==0)=min(sigma_c(~isnan(arr_timesobs)&arr_timesobs>0))*0.01;
%         
%         Cdd_c = spdiags(sigma_c.^2,0,nobs_c,nobs_c);
%         %lnKprior_c = lnKpost;
%         [lnKpost_c,c_sim,objfun_c,call,done_c] = ...
%                             inversekernel(lnKpost,data_c,Cdd_c,type,...
%                             nelflotra,info,@calculate_concentration3d,...
%                             dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
%                             nelflotra, nyel,nxel,nzelflotra,hin, hout,...
%                             Xflotra,Yflotra,Zflotra,...
%                             wellObj, n,tracer_mass,...
%                             dt, nt, store_step, n_store, which_in,...
%                             plotflag, AMGflag, datatypeflag);
%                                    
%     
%        save(strcat(test_name,'_results_transport_',addstr),'Cdd_c','sigma_c','wellObj', 'nobs_c','lnKpost_c','c_sim','objfun_c','call','done_c', 'data_c')
%     elseif ~inversion_conc
%         load(strcat(test_name,'_results_transport_',addstr))
%     end %inversion_conc
%     
%     meancall = mean(call,2);
%     meanlnKpost_c = mean(lnKpost_c,2);
%     stdlnKpost_c  = std(lnKpost_c,[],2);
% 
%     if info.plotflag
%         
%         meanlnKpostr_c = reshape(meanlnKpost_c, length(dy),length(dx), length(dzflotra));
%         figure(1)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(2,2,4)
%         h5 = slice(Xflotra,Yflotra,Zflotra,meanlnKpostr_c,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h5,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Posterior Mean of ln(K): transport')
%         caxis(ca_lnKtrue);
%         
%         stdlnKpostr_c= reshape(stdlnKpost_c, length(dy),length(dx), length(dzflotra));
%         figure(5)
%         set(gcf,'outerposition',get(0,'screensize'))
%         subplot(1,3,3)
%         h6 = slice(Xflotra,Yflotra,Zflotra,stdlnKpostr_c,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
%         set(h6,'edgecolor','none')
%         daspect([1 1 1]);
%         colormap jet
%         box on;
%         colorbar('Location','southoutside')
%         title('Posterior Standard Deviation of ln(K): transport')
%         caxis(ca_stdprior);
%         
%         figure(4)
%         normres_c=(c_sim(:)-reshape(data_c*ones(1,size(c_sim,2)),nobs_c*size(c_sim,2),1))./...
%                             reshape(sigma_c*ones(1,size(c_sim,2)),nobs_c*size(c_sim,2),1);
%         handle_c=histfit(normres_c(:));
%         set(handle_c(1),'facecolor',[.5 .5 .5],'edgecolor','none')
%         xlabel('(c_{sim}-c_{meas})/\sigma_c [-]')
%         ylabel('count')
%         set(handle_c(2),'color','k')
%         [fitmean_c,fitstd_c]=normfit(normres_c);
%         legend('final ensemble',sprintf('N(%5.2f,%5.2f)',[fitmean_c,fitstd_c]))
%     end    
% end %run_inversion
% 
% %% To get the kkk from thesis with the same number of elements
% %nodes, elements, nodes_coord, element_nodes, smallgrid_nodes, center_smallgrid_ids = sel_grid(r'\\134.2.42.9\emilio\enkf_storetemp\RefinedGrid\2d\2d_extra\model\Flow\meshtecplot.dat', [33.8,67], [(52-.2*1.5**12),(68+.2*1.5**11)], [0,1], store=False, return_val=True, fulloutput=True, gridformat = 'tp2014')
% % fig = plt.figure(1)
% % ax1 = fig.add_subplot(111)
% % im1 = ax1.imshow(np.log(kkkGaus[center_smallgrid_ids]).reshape(-1,331), interpolation='nearest', cmap=plt.get_cmap('jet'), origin='lower')
% % norm = mpl.colors.Normalize(vmin=-8, vmax=-2.7)
% % import matplotlib as mpl
% % norm = mpl.colors.Normalize(vmin=-8, vmax=-2.7)
% % im1.set_norm(norm)
% % cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
% % cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
% 
% %% Run model simulations with updated parameters:
% if run_predictions  
%    %% Forward Simulations:
%    [~,IND]=sort(objfun_c(end,done_c),2,'descend');
%    [nel,nreal_post] = size(lnKpost_c);
%    cobs_pred=nan(nobs_c,n_store,nreal_post);
%    cobs_ini=nan(nobs_c,n_store,nreal);
%    %call_pred = nan(nel, n_store, nreal_post);
% 
%    %% run the ensemble with optimized ensemble
%     plotflaf=false;
%    parfor ireal=1:nreal_post
%        %call_pred(:,:,ireal)
%        [cobs_pred(:,:,ireal), ~, ~,~,~] = calculate_concentration3d(lnKpost_c(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
%                                         nelflotra, nyel,nxel,nzelflotra,hin, hout,...
%                                         Xflotra,Yflotra,Zflotra,...
%                                         wellObj, n,tracer_mass,...
%                                         dt, nt, store_step, n_store, which_in,...
%                                         plotflag, AMGflag, 'conc');
%    end
%     poolobj = gcp('nocreate');
%     delete(pool)
%     
%     [~,nreal] = size(lnKprior)
%     parfor ireal=1:nreal
%        %call_pred(:,:,ireal)
%        [cobs_ini(:,:,ireal), ~, ~,~,~] = calculate_concentration3d(lnKprior(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
%                                         nelflotra, nyel,nxel,nzelflotra,hin, hout,...
%                                         Xflotra,Yflotra,Zflotra,...
%                                         wellObj, n,tracer_mass,...
%                                         dt, nt, store_step, n_store, which_in,...
%                                         plotflag, AMGflag, 'conc');
%    end
%    
% 
%  % rerun model with reference K field
% [cobs_ref, ~, times,~,~] = calculate_concentration3d(lnK, dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
%                                         nelflotra, nyel,nxel,nzelflotra,hin, hout,...
%                                         Xflotra,Yflotra,Zflotra,...
%                                         wellObj, n,tracer_mass,...
%                                         dt, nt, store_step, n_store, which_in,...
%                                         plotflag, AMGflag, 'conc');
%                                               
% save(strcat(test_name,'_results_posterior_sims',addstr),'IND','nreal_post','cobs_pred','cobs_ref', 'times','cobs_ini')
% elseif ~run_predictions
%    load(strcat(test_name,'_results_posterior_sims',addstr))
% end
% 
% if plotflag
%    figure(6)
%    %16, 84th percentile: one std dev
%    for ii=1:nobs_c
%        subplot(5,4,ii)
%        % plot predictions post ensemble
%        sel_obs = cumsum(squeeze(cobs_pred(ii, :, :)));
%        sel_obs = sel_obs(:,IND(1:28));
%        quant = quantile(sel_obs(:, :)',[.16 .5 .84]);
%        plot(times,quant, '-.', 'LineWidth',1.5)
%        hold on
%        % plot prediction prior ensemble
%        sel_obs_prior = cumsum(squeeze(cobs_ini(ii, :, :)));
%        quant_prior = quantile(sel_obs_prior(:, :)',[.16 .5 .84]);
%        plot(times,quant_prior, '-.', 'Color',[0 0 0]+0.5, 'LineWidth',1.5)
%        %plot reference obs
%        plot(times, cumsum(cobs_ref(ii,:)),'k', 'LineWidth',2)
%        
%        hold off
%        title(sprintf('Obs. point %d',ii))
%    end
% end