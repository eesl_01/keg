tic
clear
close all

pool=parpool('eesl1',7);
poolobj = gcp('nocreate'); % If no pool, do not create new one.
if isempty(poolobj)
    poolsize = 0;
else
    poolsize = poolobj.NumWorkers;
end

disp([datestr(clock) ': Pool size is ', num2str(poolsize)])

toc