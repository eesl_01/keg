%function [c, m0, m1] 
function [varargout] = calculate_concentration3d(lnK, dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                             nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                             Xflotra,Yflotra,Zflotra,...
                             wellObj, n,tracer_mass,...
                             dt, nt, store_step, n_store, which_in,...
                             plotflag, AMGflag, datatypeflag)
% This is the ultra-cool 3-D code solving solute transport
% in a double nested flow field mimicking the Lauswiesen site.
%
% All pdes are solved by the Finite Volume Method on cuboids.
% Upwind differentiation is applied to stabilize advection-dominated
% transport.
% If available, an algebraic multigrid preconditioner is used in solving
% the Poisson equation.
%
% The AMG preconditioner is documented in:
% J. Boyle, M. Mihajlovic, J. Scott (2010): HSL_MI20: An efficient AMG
% preconditioner for finite element problems in 3D. Internat. J. Num. Meth.
% Eng. 82(1): 64-98, doi: 10.1002/nme.2758
%
% (c) Olaf A. Cirpka
% November 30, 2018
% olaf.cirpka@uni-tuebingen.de
% unpack well nodes and rates:

%%  Solve first for steady-state heads:

[~,h, M, ~, ~]=calculate_head3d(lnK, nyel, nxel, nzelflotra,nelflotra,dx,dy,dzflotra,...
                          Xflotra,Yflotra,Zflotra, hin, hout, wellObj, ...
                          AMGflag, plotflag);

nodes_well_in1 = wellObj.nod_well.well_1;
nodes_well_in2 = wellObj.nod_well.well_2;
nodes_well_in3 = wellObj.nod_well.well_3;
nodes_well_in4 = wellObj.nod_well.well_4;
nodes_well_out1 = wellObj.nod_well.well_5;
nodes_well_out2 = wellObj.nod_well.well_6;
Qin2 =  wellObj.Q.Qin2;
Qin3 =  wellObj.Q.Qin3;
Qin4 =  wellObj.Q.Qin4;

 % compute nodal load
bc_dir=unique([nodeinflotra(:);nodeoutflotra(:);nodes_well_in1(:);nodes_well_in2(:);...
               nodes_well_in3(:);nodes_well_in4(:);nodes_well_out1(:);...
               nodes_well_out2(:)]);

loaddir=M(bc_dir,:)*h(:);
    
%% Initialize Transport
disp([datestr(clock) ': Assemble matrices for transport']);
K = reshape(exp(lnK), nyel,nxel,nzelflotra);
clear lnK
[Mmob,Mstore] = Matrices_Transport(dx,dy,dzflotra,h,K,n,[],[],[],[]);

% Diagonal matrix at outflow boundaries
diag_mat_out=zeros(nelflotra,1);
diag_mat_out(bc_dir)=0.5*(loaddir-abs(loaddir));
mat_out=spdiags(diag_mat_out,0,nelflotra,nelflotra);

Mleft                = Mmob -mat_out;
r0                   = zeros(nelflotra,1);
switch which_in
    case 'top'
         r0(nodes_well_in2)   = M(nodes_well_in2,:)*h(:)*tracer_mass;
    case 'middle'
         r0(nodes_well_in3)   = M(nodes_well_in3,:)*h(:)*tracer_mass;
    case 'bottom'
         r0(nodes_well_in4)   = M(nodes_well_in4,:)*h(:)*tracer_mass;
end

% store transport
% h5create(HDF5file,'/m0',[nyel nxel nzelflotra 1],...
%     'ChunkSize',[nyel nxel nzelflotra 1],'Deflate',9);
% h5create(HDF5file,'/m1',[nyel nxel nzelflotra 1],...
%     'ChunkSize',[nyel nxel nzelflotra 1],'Deflate',9);
% h5create(HDF5file,'/conc',[nyel nxel nzelflotra n_store],...
%     'ChunkSize',[nyel nxel nzelflotra 1],'Deflate',9);


% if contains(datatypeflag,'conc')
%   c = zeros(nelflotra,1);
%   h5write(HDF5file,'/conc',reshape(c,[nyel nxel nzelflotra]),[1 1 1 1],...
%          [nyel nxel nzelflotra 1]);

if contains(datatypeflag,'mom')
    %% Solve for Temporal Moments by Moment-Generating Equations
    disp([datestr(clock) ': Solve for zeroth moment']);
    m0=reshape(Mleft\r0,nyel,nxel,nzelflotra);
    % h5write(HDF5file,'/m0',m0,[1 1 1 1],...
    %        [nyel nxel nzelflotra 1]);
    r1=Mstore*m0(:);

    disp([datestr(clock) ': Solve for first moment']);
    m1=reshape(Mleft\r1,nyel,nxel,nzelflotra);
%     h5write(HDF5file,'/m1',m1,[1 1 1 1],...
%            [nyel nxel nzelflotra 1]);
end % contains 'conc'

if contains(datatypeflag,'conc')
    c_cur = zeros(nelflotra,1);
    c = zeros(nelflotra, n_store);
    t_all = zeros(n_store,1);
    cobs = zeros(numel(fieldnames(wellObj.nod_obs_c)), n_store);
    % Change right-hand side for transient transport
    switch which_in
        case 'top'
             r0(nodes_well_in2)   = M(nodes_well_in2,:)*h(:)*tracer_mass/Qin2/dt;
        case 'middle'
             r0(nodes_well_in3)   = M(nodes_well_in3,:)*h(:)*tracer_mass/Qin3/dt;
        case 'bottom'
             r0(nodes_well_in4)   = M(nodes_well_in4,:)*h(:)*tracer_mass/Qin4/dt;
    end
    % Change left-hand side matrix
    Mleft                = Mmob -mat_out + Mstore/dt;
    disp([datestr(clock) ': Factorize left-hand matrix for transient transport']);
    % [L,U]=ilu(Mleft);
    % full lu factorization
    [L_Mtrans,U_Mtrans,P_Mtrans,Q_Mtrans,R_Mtrans]=lu(Mleft);
    Mright               = Mstore/dt;
elseif contains(datatypeflag,'mom')
    mean_arr_times_all = m1(:)./m0(:);
    mean_arr_times_all(isnan(mean_arr_times_all))=0;
    
    if isfield(wellObj, 'nod_obs_c')
        if isstruct(wellObj.nod_obs_c)
            fields = fieldnames(wellObj.nod_obs_c);
            mean_arr_times_obs=zeros(numel(fields),1);
            for cur_field=1:numel(fields)
                mean_arr_times_obs(cur_field) = mean(mean_arr_times_all(wellObj.nod_obs_c.(fields{cur_field})));
            end
        end
    else
    mean_arr_times_obs=0;
    end %isfield(wellObj, 'nod_obs')
     varargout{1}=nan; %   cobs;
        varargout{2}=nan;  % c;
        varargout{3}=nan;
        varargout{4}=mean_arr_times_obs;
        varargout{5}=mean_arr_times_all;
end

if contains(datatypeflag,'conc')
    %% Time Loop

      i_ERT = 1;
      for it=1:nt-1
          t = dt*it;
          % disp([datestr(clock) ': Solve for concentration at t = ' num2str(t) ' s']);
          % with full factorization
          c_cur=Q_Mtrans*(U_Mtrans\(L_Mtrans\(P_Mtrans*(R_Mtrans\(r0+Mright*c_cur(:))))));
          if isfield(wellObj, 'nod_obs_c')
                if isstruct(wellObj.nod_obs_c)
                    fields = fieldnames(wellObj.nod_obs_c);
                    cobs_cur=zeros(numel(fields),1);
                    for cur_field=1:numel(fields)
                        cobs_cur(cur_field) = mean(c_cur(wellObj.nod_obs_c.(fields{cur_field})));
                    end
                end
          else
                cobs_cur=0;
          end %isfield(wellObj, 'nod_obs')
    
          % BiCGStab with ILU factorization
      %     c=bicgstab(Mleft,r0+Mright*c(:),10*eps,10*nel,L,U,c(:));
          r0 = zeros(nelflotra,1);
          % disp([datestr(clock) ': Concentration solved']);
          if mod(it,store_step)==0
             i_ERT = i_ERT+1;
             t_all(i_ERT)=t;
             cobs(:,i_ERT) = cobs_cur;
             c(:,i_ERT) = c_cur;
             % store in HDF5 file
%             h5write(HDF5file,'/conc',c,[1 1 1 i_ERT],...
%              [nyel nxel nzelflotra 1]);
            if plotflag
              plotheads(Xflotra,Yflotra,Zflotra,reshape(c(:,i_ERT),nyel,nxel,nzelflotra),max(c(:,i_ERT)),0,10);
              hold on
              plot3(Xflotra(wellObj.nod_well.well_6),Yflotra(wellObj.nod_well.well_6),Zflotra(wellObj.nod_well.well_6),'rx');
              plot3(Xflotra(wellObj.nod_well.well_5),Yflotra(wellObj.nod_well.well_5),Zflotra(wellObj.nod_well.well_5),'rx');
%               kids=get(gca,'children');
%               set(kids(3:end),'facealpha',.1)
%               axis tight
%               grid on
%               drawnow
           end
          end
      end
        varargout{1}=nan; %   cobs;
        varargout{2}=nan;  % c;
        varargout{3}=t_all;
        varargout{4}=cobs;
        varargout{5}=c;
end % if concentrations 
    %varargout{4}=nan;
    %varargout{5}=nan;
end % function

% delete(pool) % close parallel session

