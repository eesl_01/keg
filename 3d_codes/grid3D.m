function [dx,dy,dz,Xc,Yc,Zc,dzflotra,...
          Xflotra,Yflotra,Zflotra]=grid3D(zflotramin,zflotramax)

xvec=[-50 -45 -40 -37.5 -35 -32.5, (-30:-20), (-19.25:0.5:-8.25), ...
      -8.05 -7.95, (-22.5:22.5)/3, 7.95 8.05 ...
      (8.25:.5:15.25), (16:25) (27.5:2.5:40) 45 50];
yvec=[-35 -30 -25 -22.5 (-20:-10) (-9.25:.5:-7.5) ...
     (-21.5:-.5)/3 -0.05 0.05 (.5:21.5)/3 (7.5:.5:9.25) (10:20) 22.5 25 30 35];
zvec=[ -20 -15 -13 -11.5 -10.7 -10.4 -10.2 -10 (-9.85:.1:-2.15) (-2:.2:0)];

dx=diff(xvec);
dy=diff(yvec);
dz=diff(zvec);

xc = 0.5*(xvec(1:end-1)+xvec(2:end));
yc = 0.5*(yvec(1:end-1)+yvec(2:end));
zc = 0.5*(zvec(1:end-1)+zvec(2:end));

[Xc,Yc,Zc]=meshgrid(xc,yc,zc);

if nargin==2
   zvecflotra=zvec(zvec>zflotramin&zvec<zflotramax);
   dzflotra=diff(zvecflotra);
   zcflotra = 0.5*(zvecflotra(1:end-1)+zvecflotra(2:end));
   [Xflotra,Yflotra,Zflotra]=meshgrid(xc,yc,zcflotra);
end
end