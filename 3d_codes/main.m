clear
close all

% reset the global random number stream based on current time
s = RandStream('mt19937ar','Seed','shuffle');
RandStream.setGlobalStream(s);

% define the grid
dx = ones(201,1);
dy = ones(101,1);
dz = 10;

xvec=[0;cumsum(dx(:))];
yvec=[0;cumsum(dy(:))];
[X_forK,Y_forK]=meshgrid(xvec,yvec);

xc = 0.5*(xvec(1:end-1)+xvec(2:end));
yc = 0.5*(yvec(1:end-1)+yvec(2:end));

[Xc,Yc]=meshgrid(xc,yc);
nel = numel(Xc);

% observation points
ix_obs = 26:50:201;
iy_obs = 13:25:101;
[IX,IY]=meshgrid(ix_obs,iy_obs);
nod_obs = (IX-1)*length(dy)+IY;
nod_obs=nod_obs(:);
nobs = length(nod_obs); % number of observations

% node of pumping wells
nod_well = [50;150]*101+51;

clear ix_obs iy_obs IX IY xvec yvec zvec xc yc zc

% hydraulic conductivity field
minKg = 3e-4; % geometric mean
maxKg = 3e-2; % geometric mean
lx = 10;      % correlation length in number of cells x-direction
ly = 10;      % correlation length in number of cells y-direction
lz = 1;       % correlation length in number of cells z-direction
Ctype = 1;    % covariance model (1: exponential, 2: Gaussian)
sigma2 = 2;   % variance of log-conductivity
nreal = 256*2;  % number of realizations

disp([datestr(now) ': Create artificial example'])
Kg  = minKg*(rand(1)*log(maxKg/minKg));
lnK = k_generate(dx,dy,dz,lx,ly,lz,Ctype,Kg,sigma2,false);

% boundary conditions
hin  = 0;
hout = 0;
Q    = [1;-1]*1e-2; % pumping rates

% uncertainty of heads
sigma_h = 0.01;
Cdd = spdiags(sigma_h^2*ones(nobs,1),0,nobs,nobs);

% forward run with true parameters
[hobs,hall] = calculate_head(lnK,dx,dy,dz,hin,hout,nod_obs,nod_well,Q);

% add measurement error to obtain virtual measurements
data = hobs+randn(length(hobs),1)*sigma_h;

figure(1)
set(gcf,'outerposition',get(0,'screensize'))
subplot(3,3,1)
lnK=reshape(lnK,length(dy),length(dx));
pcolor(X_forK,Y_forK,[lnK lnK(:,end);lnK(end,:) lnK(end,end)])
shading flat
daspect([1 1 1])
set(gca,'layer','top')
colorbar
title('True Field of ln(K)')
ca_lnKtrue=caxis;

subplot(3,3,7)
[~,handle_lines]=contour(Xc,Yc,reshape(hall,length(dy),length(dx)),50);
hold on
plot(Xc(nod_obs),Yc(nod_obs),'kx')
plot(Xc(nod_well),Yc(nod_well),'ko')
hold off
xlim([0 sum(dx)])
ylim([0 sum(dy)])
colormap jet
daspect([1 1 1])
set(gca,'layer','top')
cacontour=caxis;
colorbar
title('True Head Field')

% generate initial ensemble
disp([datestr(now) ': Generate initial ensemble'])
lnKprior = nan(nel,nreal);
pool=parpool; % start parallel session
parfor ireal=1:nreal
    Kg  = minKg*(rand(1)*log(maxKg/minKg));
    now = k_generate(dx,dy,dz,lx,ly,lz,Ctype,Kg,sigma2,false);
    lnKprior(:,ireal) = now(:);
end

meanlnKprior = mean(lnKprior,2);
stdlnKprior = std(lnKprior,[],2);

subplot(3,3,2)
now=reshape(meanlnKprior,length(dy),length(dx));
pcolor(X_forK,Y_forK,[now now(:,end);now(end,:) now(end,end)])
caxis(ca_lnKtrue);
shading flat
daspect([1 1 1])
set(gca,'layer','top')
colorbar
title('Prior Mean of ln(K)')

subplot(3,3,5)
now=reshape(stdlnKprior,length(dy),length(dx));
pcolor(X_forK,Y_forK,[now now(:,end);now(end,:) now(end,end)])
shading flat
daspect([1 1 1])
set(gca,'layer','top')
colorbar
title('Prior Standard Deviation of ln(K)')
drawnow

%%
% type of inverse Kernel:
% 2: ES-MDA
% 3: KEG
type = 3;
% number of iterations with measurement-variance inflation
info.niter=10;
% factor of variance-inflation from one iteration to the next
info.fac_alpha=0.5;
% maximum number of KEG iterations 
info.maxiter=10;
% perturb the measured data?
info.data_perturb=true;
% scale the perturbations by sqrt(alpha_i) in ES-MDA?
info.scale_perturb_by_alpha = false;
% maximum additional variance inflation factor in KEG inner iterations
info.maxbeta=512;
% plot the CDF of the objective function
info.plotflag = true;
% return only converged realizations
info.restrict_to_converged_realizations = true;

[lnKpost,h_sim,objfun,hall,done] = inversekernel(lnKprior,data,Cdd,type,...
            nel,info,@calculate_head,dx,dy,dz,hin,hout,nod_obs,nod_well,Q);
delete(pool) % close parallel session

meanlnKpost = mean(lnKpost,2);
meanhall    = mean(hall,2);
stdlnKpost  = std(lnKpost,[],2);

figure(1)
subplot(3,3,3)
now=reshape(meanlnKpost,length(dy),length(dx));
pcolor(X_forK,Y_forK,[now now(:,end);now(end,:) now(end,end)])
caxis(ca_lnKtrue);
shading flat
daspect([1 1 1])
set(gca,'layer','top')
colorbar
title('Posterior Mean of ln(K)')

subplot(3,3,6)
now=reshape(stdlnKpost,length(dy),length(dx));
pcolor(X_forK,Y_forK,[now now(:,end);now(end,:) now(end,end)])
shading flat
hold on
plot(Xc(nod_obs),Yc(nod_obs),'wx')
hold off
daspect([1 1 1])
set(gca,'layer','top')
colorbar
title('Posterior Standard Deviation of ln(K)')

subplot(3,3,9)
contour(Xc,Yc,reshape(meanhall,length(dy),length(dx)),handle_lines.LevelList);
hold on
plot(Xc(nod_obs),Yc(nod_obs),'kx')
plot(Xc(nod_well),Yc(nod_well),'ko')
hold off
daspect([1 1 1])
xlim([0 sum(dx)])
ylim([0 sum(dy)])
set(gca,'layer','top')
caxis(cacontour);
colorbar
title('Posterior Mean Head Distribution')
drawnow

figure(3)
normres=(h_sim(:)-reshape(data*ones(1,size(h_sim,2)),nobs*size(h_sim,2),1))/sigma_h;
handle=histfit(normres);
set(handle(1),'facecolor',[.5 .5 .5],'edgecolor','none')
xlabel('(h_{sim}-h_{meas})/\sigma_h [-]')
ylabel('count')
set(handle(2),'color','k')
[fitmean,fitstd]=normfit(normres);
legend('final ensemble',sprintf('N(%5.2f,%5.2f)',[fitmean,fitstd]))
