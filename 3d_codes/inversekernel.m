function [Xpost,Y,objfun,Yall,done] =inversekernel(Xprior,data,Cdd,type,...
                                          nyall,info,forwardmodel,varargin)
% [Xpost,Y,objfun,Yall,done] = inversekernel(Xprior,data,Cdd,type,nyall,...
%                                         info,@forwardmodel,par1,par2,...)
% Inverse kernel of an ensemble-Kalman update step used for parameter
% estimation
% Xprior (nx x nreal) : prior ensemble of the parameters x
% data (ny x 1)       : measurements of dependent quantity y
% Cdd (ny x ny)       : coariance matrix of the measurement uncertainty
% type              1 : single ensemble-Kalman smoother step
%                   2 : ensemble-Kalman smoother multiple data-assimilation
%                       (ES-MDA)
%                   3 : Kalman ensemble generator (KEG)
% nyall               : size of the model output if not only the
%                       measurements are reported
% info                : structure with parameters to operate the Kalman
%                       update, depends on type
%                       niter : number of iterations with measurement-
%                               variance inflation
%                       fac_alpha: factor of variance-inflation from one
%                                  iteration to the next
%                       maxiter: maximum number of KEG iterations 
%                       data_perturb: perturb the measured data? (boolean)
%                       scale_perturb_by_alpha: scale the perturbations by
%                                                sqrt(alpha_i) in ES-MDA?
%                       maxbeta: maximum additional variance inflation
%                                factor in KEG inner iterations
%                       plotflag: plot the CDF of the objective function
%                                 (boolean)
%                       restrict_to_converged_realizations: return only
%                       converged realizations (boolean)
% @forwardmodel       : function handle to the forward model of the type
%                       y = f(x,par1,par2,...)
% par1, par2, ...     : parameters passed to the forward model
%
% Returns the following variables
% Xpost (nx x nreal)  : posterior ensemble of the parameters x
% Y (ny x nreal)      : ensemble of model outcomes y
% objfun (niter x nreal) : history of objective function
% Yall (nyall x nreal): ensemble of complete model results
% done (1 x nreal)    : has the realization converged? (boolean)

[nel,nreal] = size(Xprior);
ny = length(data);
done = true(1,nreal);

% threshold values for lnK
min_lnK = log([1e-9 1e-8]);
max_lnK = log([1e-3 1e-2]);
% Perturbing the measurements if wanted
if info.data_perturb
    % Cholesky decomposition of Cdd
    L  = chol(Cdd,'lower');
    % Generate perturbation of the data
    dpert = L*randn(ny,nreal);
else
    dpert = zeros(ny,nreal);
end

% Initialize ensemble of model outputs
Y    = nan(ny,nreal);
Yall = nan(nyall,nreal);
switch type
    case 1 % single Ensemble-Kalman Smoother step
        objfun = nan(2,nreal);
        % forward run of the entire ensemble
        parfor ireal=1:nreal
            [Y(:,ireal),Yall(:,ireal),~]=forwardmodel(Xprior(:,ireal),varargin{:});
            objfun(1,ireal)=(data(:) + dpert(:,ireal)- Y(:,ireal))'...
                   *(Cdd\(data(:) + dpert(:,ireal)- Y(:,ireal)));
        end
        % compute deviation of perturbed data and model output
        d_minus_y = data(:)*ones(1,nreal) + dpert - Y;
        % select parameters of inner zone if true
        if info.invert_inner_zone
            Xprior = Xprior(info.indices,:);
        end
        
        % compute deviations of Xprior and Y from the mean
        Xprime = Xprior - mean(Xprior,2)*ones(1,nreal);
        Yprime = Y - mean(Y,2)*ones(1,nreal);
        % Compute the covariance matrix of the model predictions
        Cyy = Yprime*Yprime';
        % Kalman update step
        Xpost = Xprior + Xprime*(Yprime'*((Cyy+Cdd)\d_minus_y));
        % forward run of the entire ensemble using the posterior
        % distribution
        if info.invert_inner_zone
            Xpost_full = zeros(nel,nreal);
            Xpost_full(info.indices,:) = Xpost;
            Xpost_full(~info.indices,:) = mean(Xpost);
            Xpost = Xpost_full;
            clear Xpost_full
        end
        parfor ireal=1:nreal
            [Y(:,ireal),Yall(:,ireal),~]=forwardmodel(Xpost(:,ireal),varargin{:});
            objfun(2,ireal)=(data(:) + dpert(:,ireal)- Y(:,ireal))'...
                   *(Cdd\(data(:) + dpert(:,ireal)- Y(:,ireal)));
        end

    case 2 % Ensemble-Kalman Smoother Multiple Data-Assimilation
        niter = info.niter;          % number of iterations
        fac_alpha = info.fac_alpha;  % factor by which the measurement-variance 
                                     % inflation factor alpha decreases
                                     % from one step to the next
        % obtain the variance-inflation factors
        alpha = var_inflat(niter,fac_alpha);
        % initialize the posterior with the prior
        Xpost = Xprior;
        objfun = nan(niter+1,nreal);
        objfunnow = nan(1,nreal);
        % vector of booleans whether set is finished
        done = false(1,nreal);
        if info.plotflag
           figure(2);clf;
        end
        % loop over all iterations
        for iter=1:niter+1
            if iter==1
               disp([datestr(now) ': Evaluate initial ensemble'])
            else
               disp([datestr(now) sprintf(': Evaluate ensemble of iteration %i',iter-1)])
            end
            % forward run of the entire ensemble
            parfor ireal=1:nreal
                if ~done(ireal)
                    if info.transport
                        varargin{11}
                    end
                    
                    [Y(:,ireal),Yall(:,ireal),~]=forwardmodel(Xpost(:,ireal),...
                      varargin{:});
                    objfunnow(ireal)=(data(:) + dpert(:,ireal) - Y(:,ireal))'...
                                    *(Cdd\(data(:) + dpert(:,ireal) - Y(:,ireal)));
                end
            end
            if info.plotflag
               plotcdf_of_objfun(objfunnow,ny);
            end
            % cdf-value of the chi2-distribution
            probcont = chi2cdf(objfunnow,length(data));
            done = done | rand(1,nreal)>probcont;
            disp(['Number of converged realizations: ' num2str(sum(done))])
            objfun(iter,:)=objfunnow;
            if iter<niter+1
               disp([datestr(now) ...
             sprintf(': measurement-variance inflated by %f',alpha(iter))])
               % compute deviation of perturbed data and model output
               if info.scale_perturb_by_alpha
                  d_minus_y=data(:)*ones(1,nreal)+dpert*sqrt(alpha(iter))-Y;
               else
                  d_minus_y = data(:)*ones(1,nreal) + dpert - Y;
               end
               % compute deviations of Xprior and Y from the mean
               Xprime = Xpost - mean(Xpost,2)*ones(1,nreal);
               Yprime = Y - mean(Y,2)*ones(1,nreal);
               % Compute the covariance matrix of the model predictions
               Cyy = Yprime*Yprime';
               % Kalman update step
               Xpost(:,~done) = Xpost(:,~done) + ...
               Xprime*(Yprime'*((Cyy+Cdd*alpha(iter))\d_minus_y(:,~done)));
            end
        end
        if info.restrict_to_converged_realizations
            Xpost=Xpost(:,done);
            Y=Y(:,done);
            Yall=Yall(:,done);
        end
        
    case 3 % Kalman Ensemble Generator
        maxiter = info.maxiter;    % maximum number of iterations
        niter   = info.niter;      % number of iterations with measurement-
                                   % variance inflation
        fac_alpha = info.fac_alpha;% factor by which the measurement-variance 
                                   % inflation factor alpha decreases
                                   % from one step to the next
        % initialize the posterior with the prior
        Xpost = Xprior;
        clear Xprior;        
        objfunnow = nan(1,nreal);
        % vector of booleans whether set is finished
        done   = false(1,nreal);
        alpha=fac_alpha^-(niter+1);
        % loop over all iterations
        % forward run of the entire ensemble
        disp([datestr(now) ': Evaluate initial ensemble'])
        
        % Constrain range of Xpost values:
        temp = Xpost < min_lnK(1);
        rand_temp = (min_lnK(2)-min_lnK(1)).*rand(size(temp)) + min_lnK(1);
        Xpost(temp) = rand_temp(temp);
        
        temp = Xpost > max_lnK(2);
        rand_temp = (max_lnK(2)-max_lnK(1)).*rand(size(temp)) + max_lnK(1);
        Xpost(temp) = rand_temp(temp);
        
        if ~info.transport
            parfor ireal=1:nreal
                   [~,~,~,Y(:,ireal),Yall(:,ireal)]=forwardmodel(Xpost(:,ireal),...
                      varargin{:});
                   objfunnow(ireal)=(data(:)+dpert(:,ireal)-Y(:,ireal))'...
                                *(Cdd\(data(:)+dpert(:,ireal)-Y(:,ireal)));
            end
        elseif info.transport
            parfor ireal=1:nreal
               [~,~,~,Y(:,ireal),Yall(:,ireal)]=forwardmodel(Xpost(:,ireal),...
                  varargin{:});
               objfunnow(ireal)=(data(:)+dpert(:,ireal)-Y(:,ireal))'...
                            *(Cdd\(data(:)+dpert(:,ireal)-Y(:,ireal)));
            end
        end
        if info.plotflag
           figure(2);clf;
           plotcdf_of_objfun(objfunnow,ny)
        end
        
        objfun=objfunnow;
        if isfield(info,'iter')
           ini_iter = info.iter;
        elseif ~isfield(info,'iter')
            ini_iter = 1;
        end
        for iter=ini_iter:maxiter
            % reduce the measuremnt-variance inflation
            alpha=max(alpha*fac_alpha,1);
            disp([datestr(now) sprintf(': outer iteration %i',iter)])
                       
            % select parameters of inner zone if true
            if info.invert_inner_zone
                Xpost = Xpost(info.inner_zone_nodes,:);
            end
            % define the new last iterate
            Xlast = Xpost;
            
            % compute deviations of Xprior and Y from the mean
            Xprime = Xpost - mean(Xpost,2)*ones(1,nreal);
            Yprime = Y - mean(Y,2)*ones(1,nreal);
            % Compute the covariance matrix of the model predictions
            Cyy = Yprime*Yprime';
            % define the new last iterate
            Ylast = Y;
            Yalllast = Yall;
            select = ~done;
            beta = 1;
            while sum(select)>0 && beta <= info.maxbeta
                  disp([datestr(now) ...
                      sprintf(': measurement-variance inflated by %f',...
                      alpha*beta)])
                  % compute deviation of perturbed data and model output
                  
                  % Fix the zeroth-valued simulations:
                  myzeros = Y == 0;
                  rtemp = randi([floor(min(data)) floor(min(data)+1000)],size(Y));
                  Y(myzeros) = rtemp(myzeros);
                  
                  d_minus_y = data(:)*ones(1,sum(select)) + ...
                              dpert(:,select) - Y(:,select);
                  % Kalman update step
                  Xpost(:,select) = Xpost(:,select) + ...
                         info.damp.*Xprime*(Yprime'*((Cyy+beta*alpha*Cdd)\d_minus_y));
                  % forward run of the entire ensemble
                  disp([datestr(now) ': Evaluate updated ensemble'])
                  
                  if info.invert_inner_zone
                        Xpost_full = zeros(nel,nreal);
                        Xpost_full(info.inner_zone_nodes,:) = Xpost;
                        Xpost_full(~info.indices,:) = mean(Xpost(:));
                        Xpost = Xpost_full;
                        clear Xpost_full
                  end
                  
                    % Constrain range of Xpost values:
                    temp = Xpost < min_lnK(1);
                    rand_temp = (min_lnK(2)-min_lnK(1)).*rand(size(temp)) + min_lnK(1);
                    Xpost(temp) = rand_temp(temp);

                    temp = Xpost > max_lnK(2);
                    rand_temp = (max_lnK(2)-max_lnK(1)).*rand(size(temp)) + max_lnK(1);
                    Xpost(temp) = rand_temp(temp);

                  if ~info.transport
                  parfor ireal=1:nreal
                     if select(ireal)
                       [~,~,~,Y(:,ireal),Yall(:,ireal)]=...
                                forwardmodel(Xpost(:,ireal),varargin{:});
                       objfunnow(ireal)=(data(:)+dpert(:,ireal)-Y(:,ireal))'...
                               *(Cdd\(data(:)+dpert(:,ireal)- Y(:,ireal)));
                     end
                
                  end
                  elseif info.transport
                      parfor ireal=1:nreal
                        if select(ireal)
                            [~,~,~,Y(:,ireal),Yall(:,ireal)]=...
                                forwardmodel(Xpost(:,ireal),varargin{:});
                       objfunnow(ireal)=(data(:)+dpert(:,ireal)-Y(:,ireal))'...
                               *(Cdd\(data(:)+dpert(:,ireal)- Y(:,ireal)));
                     end
                      end
                  end
                  
                  if info.invert_inner_zone
                    Xpost = Xpost(info.inner_zone_nodes,:);
                  end
                  % reject updates that lead to a detoriation
                  select=objfunnow>objfun(end,:);
                  disp([datestr(now) ': Number of rejected updates: ' ...
                      num2str(sum(select))])
                  Xpost(:,select)=Xlast(:,select);
                  Y(:,select)=Ylast(:,select);
                  Yall(:,select)=Yalllast(:,select);
                  objfunnow(:,select)=objfun(end,select);
                  % increase measurement-variance iflation factor
                  beta = beta*2;
                  
                  
            end
            
            if info.invert_inner_zone
                        Xpost_full = zeros(nel,nreal);
                        Xpost_full(info.inner_zone_nodes,:) = Xpost;
                        Xpost_full(~info.indices,:) = mean(Xpost(:));
                        Xpost = Xpost_full;
                        clear Xpost_full
            end
            
            info.iter = iter;
            xpost_file = fullfile(info.results_dir,strcat('Xpost_iter_',num2str(iter)));
            save(xpost_file, 'Xpost', 'info', '-v7.3')
            
%             save('Xpost_iter', 'Xpost', 'info', '-v7.3')

            if info.plotflag
               plotcdf_of_objfun(objfunnow,ny);
            end
            
            % extend history of objective function
            objfun=[objfun;objfunnow];
            % cdf-value of the chi2-distribution
            probcont = chi2cdf(objfunnow,length(data));
            done = done | rand(1,nreal)>probcont;
            disp([datestr(now) ': Number of converged realizations: ' ...
                num2str(sum(done))])
            if sum(done)==nreal
               break
            end
            
            
        end
        if info.restrict_to_converged_realizations
            Xpost=Xpost(:,done);
            Y=Y(:,done);
            Yall=Yall(:,done);
        end
end
end

function plotcdf_of_objfun(objfunnow,ny)
% graphical output of objective function
figure(2)
hold on
cdfplot(objfunnow/ny)
hold off
set(gca,'xscale','log')
xlabel('sum of scaled squared residuals / number of measurements')
ylabel('P [-]')
title('CDF of Objective Function')
box on
drawnow
end

function alpha = var_inflat(niter,fac_alpha)
    % obtain a series of alpha with niter elements with
    % sum(1/alpha_i) = 1
    % alpha(i+1) = fac_alpha*alpha(i)
    bla = ones(niter,1);
    for i=2:niter
        bla(i)=bla(i-1)/fac_alpha;
    end
    alpha=sum(bla)./bla;
end

function P=chi2cdf(val,nu)
P=gammainc(val/2,nu/2);
end
