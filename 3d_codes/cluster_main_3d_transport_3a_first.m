tic
clear
close all
%% Do DA with a 3D flow and transport model
% Generates a 3D reference scenario
% Runs either steady-state flow alone or coupled with transport
% THIS VERSION IS FOR THE ASSIMILATION OF REAL HT AND TT DATA

% Assimilation order for flow: 3b then 3a then 1b
% Assimilation order for transport: 3a then 2a then 3b
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 1:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assimilate tracer data? (run transport model?)
inversion_conc = false;
% Type of data to assimilate (only transport): moments or conc
datatypeflag = 'moments';
% Limit parameter updating to the inner model area
invert_inner_zone = true;
% If not first assimilated test, set to false and give source file name
prior_assimtest = true;

make_inirun = false;
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 2:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of realizations
nreal = 128*4;
% Test name:  options: 3dtest_3aTR then 3dtest_2aTR then 3dtest_3bTR
test_name = '3dtest_3aTR';
% Additional test identifier, will be attached to the files with the results:
addstr = '512m_30psigmac';
% Store data?
storeData = true;
% Activate plot settings?
plotflag = false;
% Number of processors and pool profile
nproc = 28;
% Run predicitions at the end of the assimialtion?
run_predictions = true;
results_dir = 'results';
results_dir_test = fullfile(results_dir,test_name);
% Needed if prior_assimtest is false:
ini_ens_file = fullfile(results_dir,'3dtest_1b', '3dtest_1b_heads_512m_03sigmah.mat');

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 3:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% percentage of measurement error to obtain virtual measurements
sigma_c_perc = 0.30; % sigma_c = 1e5;
% Boundary conditions
hin  = 0.01; %0.33;
hout = 0;
%% Define transport parameters and tracer injection
% porosity
n = 0.01;            %0.3; %0.08;
% Time discretization
dt = 60; %60;          % time increment [s]
tend = 86400; %86400;     % end time [s]
nt = tend/dt+1;   % number of time steps
store_step = 20;  % how often is c stored (every store_step-th time)
n_store=(nt-1)/store_step+1; % number of stored concentrations
%dt_store = dt*store_step;

%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 4:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% type of inverse Kernel:
% 2: ES-MDA, 3: KEG
type = 3;
% number of iterations with measurement-variance inflation
info.niter=10;
% factor of variance-inflation from one iteration to the next
info.fac_alpha=0.5;
% maximum number of KEG iterations 
info.maxiter=20;
% perturb the measured data?
info.data_perturb=true;
% scale the perturbations by sqrt(alpha_i) in ES-MDA?
info.scale_perturb_by_alpha = false;
% maximum additional variance inflation factor in KEG inner iterations
info.maxbeta=512*1;
% include damping factor?
info.damp = 1;
% plot the CDF of the objective function
info.plotflag = false;
% return only converged realizations
info.restrict_to_converged_realizations = false;
% is the forward model transport?
info.transport = false;
% inverting only the inner model zone?
info.invert_inner_zone = invert_inner_zone;
info.nprocs = nproc;
info.results_dir = results_dir_test;

%% Set path to AMG preconditioner and check whether the mex-file can be found
% addpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
% javaaddpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
% addpath('/home/olaf/hsl_mi20-2.0.0/matlab/')
% javaaddpath('/home/olaf/hsl_mi20-2.0.0/matlab/')
addpath('/beegfs/work/tu_emgsa01/hsl_mi20-2.0.0/matlab/')
javaaddpath('/beegfs/work/tu_emgsa01/hsl_mi20-2.0.0/matlab/')

mkdir(results_dir_test);
addpath(strcat('./', results_dir))
addpath(strcat('./', results_dir_test))

if exist('hsl_mi20','file')==3
   AMGflag = true;
   disp([datestr(clock) ': AMG preconditioner found']);
else
   AMGflag = false;
   disp([datestr(clock) ': No AMG preconditioner found, work with ILU']);
end

%% reset the global random number stream based on current time
s = RandStream('mt19937ar','Seed','shuffle');
RandStream.setGlobalStream(s);

%% Define the grid
zflotramin = -10.01;%-9.01;
zflotramax = -1.99; %-2.99;
disp([datestr(now) ': Creating model grid'])
% build the grid: coordinates of nodes
[dx,dy,dz,Xc,Yc,Zc,dzflotra,Xflotra,Yflotra,Zflotra]=...
    grid3D(zflotramin,zflotramax);
% number of elements in all directions
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);
nzelflotra=length(dzflotra);
nel=nxel*nyel*nzel;
nelflotra=nxel*nyel*nzelflotra;

% mapper indices large domain -> flow-and-transport domain
z_map_flotra = findnodes(Xc(1,1,:),Yc(1,1,:),Zc(1,1,:),...
                         Xc(1)+[-.01 0.01],Yc(1)+[-0.01 0.01],...
                         [zflotramin zflotramax]);
% node numbers in 3-D array
elnum=reshape(1:nxel*nyel*nzel,nyel,nxel,nzel);
elnumflotra=reshape(1:nxel*nyel*nzelflotra,nyel,nxel,nzelflotra);

% cells at the in- and outflow faces
nodein  = elnum(:,1,:);nodein=nodein(:);
nodeout = elnum(:,nxel,:);nodeout=nodeout(:);
nodeinflotra  = elnumflotra(:,1,:);nodeinflotra=nodeinflotra(:);
nodeoutflotra = elnumflotra(:,nxel,:);nodeoutflotra=nodeoutflotra(:);

%% Define stuff specific for each test:
% Observation points and pumping wells
% Define the wells            
% well_1: outer injection well          well_4: inner injection well bottom
% well_2: inner injection well top      well_5: inner extraction well   
% well_3: inner injection well middle   well_6: outer extraction well     

%% Injection and extraction rates [m3/s]
% Injected mass of tracer [kg] : test 3a: 1.1e-3; test2a: 1.1e-3; test3b: 1.15e-3
% Test 3b:
if isequal(test_name,'3dtest_3bTR') 
    % which screen is the injection screen ('top','middle','bottom')
    which_in='bottom';
    tracer_mass = 1.15e-3;
    wellObj.Q = struct('Qin1', 4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.82e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -8.7e-3);
    well_file = './field_data/test_wells3b_TRANSP.txt';
    obsfile = './field_data/test_3b_01072016_arrivaltimes.txt';
    
% Test 3a: 22062016
elseif isequal(test_name,'3dtest_3aTR')
    % which screen is the injection screen ('top','middle','bottom')
    which_in='bottom';
    tracer_mass = 1.1e-3;
    wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.83e-3, 'Qin3', 0.9e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.5e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells3a_TRANSP.txt';
    obsfile = './field_data/test_3a_22062016_arrivaltimes.txt';

% Test 2a: 
elseif isequal(test_name,'3dtest_2aTR')
    % which screen is the injection screen ('top','middle','bottom')
    which_in='middle';
    tracer_mass = 1.1e-3;
    wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.83e-3, 'Qin3', 0.9e-3,...
                  'Qin4', 0.6e-3, 'Qout1', -2.5e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells2a_TRANSP.txt';
    obsfile = './field_data/test_2a_10062016_arrivaltimes.txt';
end

% nodes belonging to the injection and extraction screens
rangeXwells = [-18.01 -17.99; -8.01  -7.99; -8.01  -7.99; -8.01  -7.99; 7.99  8.01; 12.99 13.01];
rangeYwells = [-0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01];
rangeZwells = [-9.01 -2.99; -4.51 -3.49;  -6.51 -5.49; -8.51 -7.99; -9.01 -2.99; -9.01 -2.99];
wellObj.nod_well = findnodes_many(Xflotra,Yflotra,Zflotra,...
                           rangeXwells,rangeYwells,rangeZwells);
% nodes belonging to the observation wells
% 1 -- 6 -- 11 -- 16
% |    |     |    |             xflat = Xflotra(:,:,1);
% 2 -- 7 -- 12 -- 17            yflat= Yflotra(:,:,1);
% |    |     |    |             plot(xflat(:), yflat(:), '.')
% 3 -- 8 -- 13 -- 18           
% |    |     |    |
% 4 -- 9 -- 14 -- 19
% |    |     |    |
% 5 -- 10 -- 15 -- 20
% rangeXwells_obs = [-5.01 -4.99;  -5.01 -4.99; -5.01 -4.99; -5.01 -4.99; -5.01 -4.99];
% rangeYwells_obs = [4.99 5.01;    1.6     1.7;  -0.01 0.01; -1.7 -1.6;  -5.01 -4.99];
% rangeZwells_obs = [-10.01 -1.99; -2.3 -2.1;  -10.01 -1.99; -10.01 -1.99;  -10.01 -1.99];
% arriv_obs_file = '';

% load observation well ranges from file
fid = fopen(well_file);
data = textscan(fid, '%f %f %f %f %f %f%*[^\n]','HeaderLines',1);
fid = fclose(fid);

rangeXwells_obs = [data{1} data{2}];
rangeYwells_obs = [data{3} data{4}];
rangeZwells_obs = [data{5} data{6}];

wellObj.nod_obs = findnodes_many(Xflotra,Yflotra,Zflotra,...
                            rangeXwells_obs, rangeYwells_obs, rangeZwells_obs);
                            
wellObj.nod_obs_c = wellObj.nod_obs;
% number of observations
fields = fieldnames(wellObj.nod_obs);
nobs = numel(fields);            
if invert_inner_zone
    inner_zone_nodes = findnodes_many(Xflotra,Yflotra,Zflotra, [-19.26, 15.26], [-9.26, 9.26],[-9.01 -2.99]);
end

%% Load real data:
fid = fopen(obsfile);
arr_timesobs_ref = textscan(fid, '%f%*[^\n]','HeaderLines',1);
fid = fclose(fid);
arr_timesobs_ref = arr_timesobs_ref{1};
    
if invert_inner_zone
    indices = zeros(nelflotra,1);
    indices(inner_zone_nodes) = 1;
%         lnK(~indices)=mean(lnK);
else
    indices = false;
    inner_zone_nodes = false;
end

if prior_assimtest
    load(ini_ens_file, 'lnKpost');     % Check if this does not override the already defined variables.
    lnKprior = lnKpost;
    clear lnKpost
elseif ~prior_assimtest
    load(fullfile(results_dir,'_ini_ensemble'), 'lnKprior')
end % prior_assimtest

meanlnKprior = mean(lnKprior,2);
stdlnKprior = std(lnKprior,[],2);

if make_inirun
%    cur_real = randsample(nreal, 1);
   [~, ~, ~,arr_timesobs, ~] = calculate_concentration3d(meanlnKprior, dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                        nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                        Xflotra,Yflotra,Zflotra,...
                                        wellObj, n,tracer_mass,...
                                        dt, nt, store_step, n_store, which_in,...
                                        plotflag, AMGflag, datatypeflag);
                                    
   
    Lx=sum(dx);
    Ly=sum(dy);
    Lz=sum(dzflotra);
    lnKr = reshape(meanlnKprior(:), length(dy),length(dx), length(dzflotra));
    
    figure(1)
    nzorg=length(dzflotra);
    nx=length(dx);
    ny=length(dy);
    nxorg=nx;
    nyorg=ny;
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(1,2,1)
    [Xex,Yex,Zex]=meshgrid(cumsum([0 dx])-sum(dx)*.5,...
                           cumsum([0 dy])-sum(dy)*.5,...
                           cumsum([0 dzflotra])-sum(dzflotra));
    ranex=zeros(size(Xex));
    ranex(1:ny,1:nx,1:length(dzflotra))=lnKr;   
    ranex = ranex(1:length(dy)+1,1:length(dx)+1,1:nzorg+1);   
    slice(Xex,Yex,Zex,ranex,[-.25 .25]*Lx,[],[-.75 -.25]*Lz);  
    hold on
    plot3(-.25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
    plot3( .25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
    plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.25*[1 1 1 1 1]*Lz,'k')
    plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.75*[1 1 1 1 1]*Lz,'k')
    plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
    plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
    plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
    plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
    hold off
    clear Xex Yex Zex ranex
    daspect([1 1 1]);
    colormap jet
    box on;
    set(gca,'xgrid','on','ygrid','on','zgrid','on','fontsize',12);
    set(gca,'clim',sqrt(sigma2)/log(10)*3*[-1 1]+log10(exp(mean(meanlnKprior))));
    xlabel('x_1');ylabel('x_2');zlabel('x_3');
    axis tight
    shading flat;
    title('log-conductivity field')
    colorbar
    set(gcf,'paperunits','centimeters','paperposition',[1 1 20 10])
    drawnow;

    subplot(1,2,2); hold on;
    plot(arr_timesobs_ref, arr_timesobs, 'ok');
    daspect([1 1 1]);
%     xlim([-0.05,0.3]);
%     ylim([-0.05,0.3]);
    grid on;
end % make_inirun

%% Initialize inversion:
if info.invert_inner_zone
    info.indices= indices;
    info.inner_zone_nodes = inner_zone_nodes;
end
resfile = fullfile(results_dir_test, strcat(test_name,'_transp_',addstr));
if inversion_conc
        
    disp([datestr(clock) ': Assimilate mean arrival times']);

    %%arr_timesobs(arr_timesobs==0) = 1e10;
    % Treat the real data prior to assimilation:
    sigma_c = sigma_c_perc.*arr_timesobs_ref;
    data_c = arr_timesobs_ref;
    %data_c = arr_timesobs_ref+randn(length(arr_timesobs_ref),1).*sigma_c;
    %sigma_c = sigma_c(~isnan(data_c)&data_c>0);
    temp = fieldnames(wellObj.nod_obs_c);
    temp = temp(~isnan(data_c)&data_c>0);

    data_c = data_c(~isnan(data_c)&data_c>0);

    for cur_field=1:numel(temp)
        temp2.nod_obs_c.(temp{cur_field}) = wellObj.nod_obs_c.(temp{cur_field});
    end
    wellObj.nod_obs_c= temp2.nod_obs_c;

    nobs_c=length(data_c);
    % Cdd_c = spdiags(sigma_c^2*ones(nobs_c,1),0,nobs_c,nobs_c);
    %sigma_c(sigma_c==0)=min(sigma_c(~isnan(arr_timesobs)&arr_timesobs>0))*0.01;

    Cdd_c = spdiags(sigma_c.^2,0,nobs_c,nobs_c);
    [lnKpost_c,c_sim,objfun_c,call,done_c] = ...
                        cluster_inversekernel(lnKprior,data_c,Cdd_c,type,...
                        nelflotra,info,@calculate_concentration3d,...
                        dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                        nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                        Xflotra,Yflotra,Zflotra,...
                        wellObj, n,tracer_mass,...
                        dt, nt, store_step, n_store, which_in,...
                        plotflag, AMGflag, datatypeflag);

    save(resfile,'Cdd_c','sigma_c','wellObj', 'nobs_c','lnKpost_c','c_sim','objfun_c','call','done_c', 'data_c', '-v7.3')
    save(strcat(resfile,'_full.mat'),'-v7.3')
    disp([datestr(clock) ': Finished assimilating mean arrival times'])

elseif ~inversion_conc
    load(resfile)
end % inversion_conc
    
meancall = mean(call,2);
meanlnKpost_c = mean(lnKpost_c,2);
stdlnKpost_c  = std(lnKpost_c,[],2);

%% Run modelpredictions:
if run_predictions
    disp([datestr(clock) ': Running model predictions'])
    %% Forward Simulations:
    [~,IND]=sort(objfun_c(end,done_c),2,'descend');
    [nel,nreal_post] = size(lnKpost_c);
    cobs_pred=nan(nobs_c,n_store,nreal_post);
    mean_arr_times_pred= nan(nobs_c,nreal_post);
    
    cobs_ini=nan(nobs_c,n_store,nreal);
    mean_arr_times_ini= nan(nobs_c,nreal_post);
    %call_pred = nan(nel, n_store, nreal_post);

    %% run the ensemble with optimized ensemble
    plotflag=false;
    defaultProfile = parallel.defaultClusterProfile;
    myCluster = parcluster(defaultProfile);
    pool = parpool(myCluster, nproc);
    parfor ireal=1:nreal_post
       %call_pred(:,:,ireal)
       [~, ~, ~, cobs_pred(:,:,ireal),~] = calculate_concentration3d(lnKpost_c(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                        nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                        Xflotra,Yflotra,Zflotra,...
                                        wellObj, n,tracer_mass,...
                                        dt, nt, store_step, n_store, which_in,...
                                        plotflag, AMGflag, 'conc');
    end
    poolobj = gcp('nocreate');
    delete(pool)
        
    myCluster = parcluster(defaultProfile);
    pool = parpool(myCluster, nproc);
    parfor ireal=1:nreal
       [~, ~, ~,cobs_ini(:,:,ireal),~] = calculate_concentration3d(lnKprior(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                    nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                    Xflotra,Yflotra,Zflotra,...
                                    wellObj, n,tracer_mass,...
                                    dt, nt, store_step, n_store, which_in,...
                                    plotflag, AMGflag, 'conc');
    end
    poolobj = gcp('nocreate');
    delete(pool)
    
    myCluster = parcluster(defaultProfile);
    pool = parpool(myCluster, nproc);
    parfor ireal=1:nreal_post
        [~, ~, ~, mean_arr_times_pred(:,ireal),~] = calculate_concentration3d(lnKpost_c(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                    nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                    Xflotra,Yflotra,Zflotra,...
                                    wellObj, n,tracer_mass,...
                                    dt, nt, store_step, n_store, which_in,...
                                    plotflag, AMGflag, 'moments');
    end
    poolobj = gcp('nocreate');
    delete(pool)

    myCluster = parcluster(defaultProfile);
    pool = parpool(myCluster, nproc);
    parfor ireal=1:nreal_post
        [~, ~, ~,mean_arr_times_ini(:,ireal),~] = calculate_concentration3d(lnKprior(:,ireal), dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                Xflotra,Yflotra,Zflotra,...
                                wellObj, n,tracer_mass,...
                                dt, nt, store_step, n_store, which_in,...
                                plotflag, AMGflag, 'moments');
                                
                                    
    end
    poolobj = gcp('nocreate');
    delete(pool)

 % rerun model with the initial K field (even before DA of drawdown):
    load(fullfile(results_dir,'_ini_ensemble'))
    lnK = mean(lnKprior,2);
    [~, ~, times, cobs_ref, ~] = calculate_concentration3d(lnK, dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                            nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                            Xflotra,Yflotra,Zflotra,...
                                            wellObj, n,tracer_mass,...
                                            dt, nt, store_step, n_store, which_in,...
                                            plotflag, AMGflag, 'conc');
                                        
    [~, ~, ~, mean_arr_times_ref, ~] = calculate_concentration3d(lnK, dx, dy, dzflotra, nodeinflotra, nodeoutflotra,...
                                        nelflotra, nyel,nxel,nzelflotra,hin, hout,...
                                        Xflotra,Yflotra,Zflotra,...
                                        wellObj, n,tracer_mass,...
                                        dt, nt, store_step, n_store, which_in,...
                                        plotflag, AMGflag, 'moments');

    save(strcat(resfile,'pred'), 'IND','nreal_post','cobs_pred', 'cobs_ini', 'cobs_ref', ...
                                 'mean_arr_times_ini', 'mean_arr_times_pred', 'mean_arr_times_ref', ...
                                 'times','cobs_ini', '-v7.3')
    
    disp([datestr(clock) ': Finish model predictions'])
    
end % run_predictions
    
% if plotflag
%    figure(6)
%    % 16, 84th percentile: one std dev
%    for ii=1:nobs_c
%        subplot(5,4,ii)
%        plot predictions post ensemble
%        sel_obs = cumsum(squeeze(cobs_pred(ii, :, :)));
%        sel_obs = sel_obs(:,IND(1:28));
%        quant = quantile(sel_obs(:, :)',[.16 .5 .84]);
%        plot(times,quant, '-.', 'LineWidth',1.5)
%        hold on
%        plot prediction prior ensemble
%        sel_obs_prior = cumsum(squeeze(cobs_ini(ii, :, :)));
%        quant_prior = quantile(sel_obs_prior(:, :)',[.16 .5 .84]);
%        plot(times,quant_prior, '-.', 'Color',[0 0 0]+0.5, 'LineWidth',1.5)
%        plot reference obs
%        plot(times, cumsum(cobs_ref(ii,:)),'k', 'LineWidth',2)
%        
%        hold off
%        title(sprintf('Obs. point %d',ii))
%    end
% end
toc
%% To get the kkk from thesis with the same number of elements
%nodes, elements, nodes_coord, element_nodes, smallgrid_nodes, center_smallgrid_ids = sel_grid(r'\\134.2.42.9\emilio\enkf_storetemp\RefinedGrid\2d\2d_extra\model\Flow\meshtecplot.dat', [33.8,67], [(52-.2*1.5**12),(68+.2*1.5**11)], [0,1], store=False, return_val=True, fulloutput=True, gridformat = 'tp2014')
% fig = plt.figure(1)
% ax1 = fig.add_subplot(111)
% im1 = ax1.imshow(np.log(kkkGaus[center_smallgrid_ids]).reshape(-1,331), interpolation='nearest', cmap=plt.get_cmap('jet'), origin='lower')
% norm = mpl.colors.Normalize(vmin=-8, vmax=-2.7)
% import matplotlib as mpl
% norm = mpl.colors.Normalize(vmin=-8, vmax=-2.7)
% im1.set_norm(norm)
% cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)
% cb1 = fig.colorbar(im1, orientation='horizontal', fraction=0.08, pad=0.1)