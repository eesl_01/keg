function [hobs,hall, varargout] = calculate_head(lnK,dx,dy,dz,hin,hout,nod_obs,nod_well,Q)
[M,Mmod,r] = Mmob_gw(dx,dy,dz,exp(lnK),hin,hout);
r(nod_well)= Q;
[L,U]=ilu(Mmod);
warning off
[hall,~]=bicgstab(Mmod,r,1e-10,10*numel(lnK),L,U);
warning on
hobs = hall(nod_obs(:));
varargout{1}=M;