function [h_obs,h, varargout]=calculate_head3d(lnK, nyel, nxel, nzelflotra,nelflotra,dx,dy,dzflotra,...
                          Xflotra,Yflotra,Zflotra, hin, hout, wellObj, ...
                          AMGflag, plotflag)
%                       (lnK, nyel, nxel, nzelflotra,nelflotra,...
%                                                     dx,dy,dzflotra,...
%                                                     Xflotra,Yflotra,Zflotra,...
%                                                     hin, hout, wellObj,AMGflag, ...
%                                                     plotflag);
%% This is the ultra-cool code for solving flow in a double
% nested flow field mimicking the Lauswiesen site.
% (c) Olaf A. Cirpka
% November 30, 2018
% olaf.cirpka@uni-tuebingen.de
% Modifications by EESL, Tubingen 12.2018

% unpack well nodes:
nodes_well_in1 = wellObj.nod_well.well_1;
nodes_well_in2 = wellObj.nod_well.well_2;
nodes_well_in3 = wellObj.nod_well.well_3;
nodes_well_in4 = wellObj.nod_well.well_4;
nodes_well_out1 = wellObj.nod_well.well_5;
nodes_well_out2 = wellObj.nod_well.well_6;

Qin1 = wellObj.Q.Qin1;
Qin2 = wellObj.Q.Qin2;
Qin3=  wellObj.Q.Qin3;
Qin4 = wellObj.Q.Qin4;
Qout1 = wellObj.Q.Qout1 ;
Qout2 = wellObj.Q.Qout2;

K = reshape(exp(lnK), nyel,nxel,nzelflotra);

%disp([datestr(clock) ': Solving flow: member ' num2str(cur_member)]);
% evaluate stiffnes matrix and right-hand side vector
% disp([datestr(clock) ': Assemble stiffness matrix for flow']);
[M, Mmod,r] = Mmob_gw(dx,dy,dzflotra,K,hin,hout);
r_for_drawdown=zeros(nelflotra,1);
Mmods = Mmod;

% put injection and extraction rates into right-hand side vector
r(nodes_well_in1(1)) = Qin1;
r(nodes_well_in2(1)) = Qin2;
r(nodes_well_in3(1)) = Qin3;
r(nodes_well_in4(1)) = Qin4;
r(nodes_well_out1(1))= Qout1;
r(nodes_well_out2(1))= Qout2;
%
r_for_drawdown(nodes_well_in1(1)) = -Qin1;
r_for_drawdown(nodes_well_in2(1)) = -Qin2;
r_for_drawdown(nodes_well_in3(1)) = -Qin3;
r_for_drawdown(nodes_well_in4(1)) = -Qin4;
r_for_drawdown(nodes_well_out1(1))= -Qout1;
r_for_drawdown(nodes_well_out2(1))= -Qout2;

% set short-cut boundary conditions
%disp([datestr(clock) ': Short-cutting the well cells']);
[Mmod,rmod]=gwshortcut(Mmod,   r,nodes_well_in1);
[Mmod,rmod]=gwshortcut(Mmod,rmod,nodes_well_in2);
[Mmod,rmod]=gwshortcut(Mmod,rmod,nodes_well_in3);
[Mmod,rmod]=gwshortcut(Mmod,rmod,nodes_well_in4);
[Mmod,rmod]=gwshortcut(Mmod,rmod,nodes_well_out1);
[Mmod,rmod]=gwshortcut(Mmod,rmod,nodes_well_out2);
%
[Mmods,rmods]=gwshortcut(Mmods,r_for_drawdown,nodes_well_in1);
[Mmods,rmods]=gwshortcut(Mmods,rmods,nodes_well_in2);
[Mmods,rmods]=gwshortcut(Mmods,rmods,nodes_well_in3);
[Mmods,rmods]=gwshortcut(Mmods,rmods,nodes_well_in4);
[Mmods,rmods]=gwshortcut(Mmods,rmods,nodes_well_out1);
[Mmods,rmods]=gwshortcut(Mmods,rmods,nodes_well_out2);

% solve for heads
if ~AMGflag
   [L,U]=ilu(Mmod);
   h=bicgstab(Mmod,rmod,1e-10,10*nelflotra,L,U);
else
   hsl_mi20_startup
   AMG_control = hsl_mi20_control;
   AMG_control.c_fail=2;
   AMG_control.st_parameter=0.75;
   AMG_control.st_method=1;
   inform = hsl_mi20_setup(Mmod,AMG_control);
   h = pcg(Mmod,rmod,1e-10,10*nelflotra,@hsl_mi20_precondition);
   hsl_mi20_finalize;
end
% solve for drawdown
if ~AMGflag
   [L,U]=ilu(Mmods);
   s=bicgstab(Mmods,rmods,1e-10,10*nelflotra,L,U);
else
   hsl_mi20_startup
   AMG_control = hsl_mi20_control;
   AMG_control.c_fail=2;
   AMG_control.st_parameter=0.75;
   AMG_control.st_method=1;
   inform = hsl_mi20_setup(Mmods,AMG_control);
   s = pcg(Mmods,rmods,1e-8,10*nelflotra,@hsl_mi20_precondition);
   hsl_mi20_finalize;
end

% copy values of short-cut nodes
h(nodes_well_in1)=h(nodes_well_in1(1));
h(nodes_well_in2)=h(nodes_well_in2(1));
h(nodes_well_in3)=h(nodes_well_in3(1));
h(nodes_well_in4)=h(nodes_well_in4(1));
h(nodes_well_out1)=h(nodes_well_out1(1));
h(nodes_well_out2)=h(nodes_well_out2(1));
%
s(nodes_well_in1)=s(nodes_well_in1(1));
s(nodes_well_in2)=s(nodes_well_in2(1));
s(nodes_well_in3)=s(nodes_well_in3(1));
s(nodes_well_in4)=s(nodes_well_in4(1));
s(nodes_well_out1)=s(nodes_well_out1(1));
s(nodes_well_out2)=s(nodes_well_out2(1));

% Extract drawdown from observation points
if isfield(wellObj, 'nod_obs')
    if isstruct(wellObj.nod_obs)
        fields = fieldnames(wellObj.nod_obs);
        s_obs=zeros(numel(fields),1);
        h_obs=zeros(numel(fields),1);
        for cur_field=1:numel(fields)
            s_obs(cur_field) = mean(s(wellObj.nod_obs.(fields{cur_field})));
            h_obs(cur_field) = mean(h(wellObj.nod_obs.(fields{cur_field})));
        end
    end
else
    s_obs=0;
    h_obs=0;
end
% h5write(modout_file,sprintf('/head_%04d',cur_member),h);
% h5create(modout_file,sprintf('/head_%04d',cur_member),[nyel nxel nzelflotra]);
% h5write(modout_file,sprintf('/head_%04d',cur_member),s);
%disp([datestr(clock) ': Heads and drawdown solved']);
if plotflag
    figure(20)
    % reshape to 3-D array
    h=reshape(h,nyel,nxel,nzelflotra);
    s=reshape(s,nyel,nxel,nzelflotra);
    %disp([datestr(clock) ': Plot heads']); 
    plotheads(Xflotra,Yflotra,Zflotra,h,hin,hout,10);
    h = h(:);
end

clear Mmod Mmods r_for_drawdown rmod;
varargout{1}=M;
varargout{2}=s_obs;
varargout{3}=s;
end
