tic
clear
close all
%% Do DA with a 3D flow and transport model
% Generates a 3D reference scenario
% Runs either steady-state flow alone or coupled with transport
% THIS VERSION IS FOR THE ASSIMILATION OF REAL HT AND TT DATA

% Assimilation order for flow: 3b then 3a then 1b
% Assimilation order for transport: 3a then 2a then 3b
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 1:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (only flow) Assimilate hydraulic head data? 
inversion_heads = true;
% Limit parameter updating to the inner model area
invert_inner_zone = true;
% If not first assimilated test, 
prior_assimtest = false;
% Needed if prior_assimtest is false and gen_ensemble is false:
ini_ens_file = 'ini_ensemble';
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 2:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of realizations
nreal = 1*4;
% Test name:  options: 3dtest_3b, 3dtest_3a, 3dtest_1b, 3dtest_3aTR then 3dtest_2aTR then 3dtest_3bTR
test_name = '3dtest_3b';
% Additional test identifier, will be attached to the files with the results:
addstr = '512m_03sigmah';
% Store data?
storeData = true;
% Activate plot settings?
plotflag = false;
% Number of processors and pool profile
pool_profile = 'local';
nproc = 20;
% Run predicitions at the end of the assimialtion?
run_predictions = true;
results_dir = 'results';
results_dir_test = fullfile(results_dir,test_name);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 3:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uncertainty of heads
sigma_h = 0.03; %0.01;
%% Boundary conditions
hin  = 0.01; %0.33;
hout = 0;
Ctype = 1;    % covariance model (1: exponential, 2: Gaussian)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 4:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% type of inverse Kernel:
% 2: ES-MDA, 3: KEG
type = 3;
% number of iterations with measurement-variance inflation
info.niter=10;
% factor of variance-inflation from one iteration to the next
info.fac_alpha=0.5;
% maximum number of KEG iterations 
info.maxiter=20;
% perturb the measured data?
info.data_perturb=true;
% scale the perturbations by sqrt(alpha_i) in ES-MDA?
info.scale_perturb_by_alpha = false;
% maximum additional variance inflation factor in KEG inner iterations
info.maxbeta=512*1;
% include damping factor?
info.damp = 1;
% plot the CDF of the objective function
info.plotflag = false;
% return only converged realizations
info.restrict_to_converged_realizations = false;
% is the forward model transport?
info.transport = false;
% inverting only the inner model zone?
info.invert_inner_zone = invert_inner_zone;
info.nprocs = nproc;
info.poolprofile = pool_profile;
info.results_dir = results_dir_test;
%% Set path to AMG preconditioner and check whether the mex-file can be found
addpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
javaaddpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
addpath('/home/olaf/hsl_mi20-2.0.0/matlab/')
javaaddpath('/home/olaf/hsl_mi20-2.0.0/matlab/')

mkdir(results_dir_test);
addpath(strcat('./', results_dir))
addpath(strcat('./', results_dir_test))

if exist('hsl_mi20','file')==3
   AMGflag = true;
   disp([datestr(clock) ': AMG preconditioner found']);
else
   AMGflag = false;
   disp([datestr(clock) ': No AMG preconditioner found, work with ILU']);
end

%% reset the global random number stream based on current time
s = RandStream('mt19937ar','Seed','shuffle');
RandStream.setGlobalStream(s);

%% Define the grid
zflotramin = -10.01;%-9.01;
zflotramax = -1.99; %-2.99;
disp([datestr(now) ': Creating model grid'])
% build the grid: coordinates of nodes
[dx,dy,dz,Xc,Yc,Zc,dzflotra,Xflotra,Yflotra,Zflotra]=...
    grid3D(zflotramin,zflotramax);
% number of elements in all directions
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);
nzelflotra=length(dzflotra);
nel=nxel*nyel*nzel;
nelflotra=nxel*nyel*nzelflotra;

% mapper indices large domain -> flow-and-transport domain
z_map_flotra = findnodes(Xc(1,1,:),Yc(1,1,:),Zc(1,1,:),...
                         Xc(1)+[-.01 0.01],Yc(1)+[-0.01 0.01],...
                         [zflotramin zflotramax]);
% node numbers in 3-D array
elnum=reshape(1:nxel*nyel*nzel,nyel,nxel,nzel);
elnumflotra=reshape(1:nxel*nyel*nzelflotra,nyel,nxel,nzelflotra);

% cells at the in- and outflow faces
nodein  = elnum(:,1,:);nodein=nodein(:);
nodeout = elnum(:,nxel,:);nodeout=nodeout(:);
nodeinflotra  = elnumflotra(:,1,:);nodeinflotra=nodeinflotra(:);
nodeoutflotra = elnumflotra(:,nxel,:);nodeoutflotra=nodeoutflotra(:);

%% Define stuff specific for each test:
% Observation points and pumping wells
% Define the wells            
% well_1: outer injection well          well_4: inner injection well bottom
% well_2: inner injection well top      well_5: inner extraction well   
% well_3: inner injection well middle   well_6: outer extraction well     

%% Injection and extraction rates [m3/s]
% Test 3b:
if isequal(test_name,'3dtest_3b')
    wellObj.Q = struct('Qin1', 4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.82e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -8.7e-3);
    well_file = './field_data/test_wells3b.txt';
    obsfile = './field_data/drdwn_3b.txt';
% Test 3a: 22062016
elseif isequal(test_name,'3dtest_3a')
    wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.83e-3, 'Qin3', 0.9e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.5e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells3a.txt';
    obsfile = './field_data/drdwn_3a.txt';

% Test 1b: 
elseif isequal(test_name,'3dtest_1b')
    wellObj.Q = struct('Qin1',4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.87e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.4e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells1b.txt';
    obsfile = './field_data/drdwn_1b.txt';
end

% nodes belonging to the injection and extraction screens
rangeXwells = [-18.01 -17.99; -8.01  -7.99; -8.01  -7.99; -8.01  -7.99; 7.99  8.01; 12.99 13.01];
rangeYwells = [-0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01];
rangeZwells = [-9.01 -2.99; -4.51 -3.49;  -6.51 -5.49; -8.51 -7.99; -9.01 -2.99; -9.01 -2.99];
wellObj.nod_well = findnodes_many(Xflotra,Yflotra,Zflotra,...
                           rangeXwells,rangeYwells,rangeZwells);
% nodes belonging to the observation wells
% 1 -- 6 -- 11 -- 16
% |    |     |    |             xflat = Xflotra(:,:,1);
% 2 -- 7 -- 12 -- 17            yflat= Yflotra(:,:,1);
% |    |     |    |             plot(xflat(:), yflat(:), '.')
% 3 -- 8 -- 13 -- 18           
% |    |     |    |
% 4 -- 9 -- 14 -- 19
% |    |     |    |
% 5 -- 10 -- 15 -- 20
% rangeXwells_obs = [-5.01 -4.99;  -5.01 -4.99; -5.01 -4.99; -5.01 -4.99; -5.01 -4.99];
% rangeYwells_obs = [4.99 5.01;    1.6     1.7;  -0.01 0.01; -1.7 -1.6;  -5.01 -4.99];
% rangeZwells_obs = [-10.01 -1.99; -2.3 -2.1;  -10.01 -1.99; -10.01 -1.99;  -10.01 -1.99];
% arriv_obs_file = '';

% load observation well ranges from file
fid = fopen(well_file);
data = textscan(fid, '%f %f %f %f %f %f%*[^\n]','HeaderLines',1);
fid = fclose(fid);

rangeXwells_obs = [data{1} data{2}];
rangeYwells_obs = [data{3} data{4}];
rangeZwells_obs = [data{5} data{6}];

wellObj.nod_obs = findnodes_many(Xflotra,Yflotra,Zflotra,...
                            rangeXwells_obs, rangeYwells_obs, rangeZwells_obs);
                            
wellObj.nod_obs_c = wellObj.nod_obs;
% number of observations
fields = fieldnames(wellObj.nod_obs);
nobs = numel(fields);            
if invert_inner_zone
    inner_zone_nodes = findnodes_many(Xflotra,Yflotra,Zflotra, [-19.26, 15.26], [-9.26, 9.26],[-9.01 -2.99]);
end

if invert_inner_zone
    indices = zeros(nelflotra,1);
    indices(inner_zone_nodes) = 1;
%         lnK(~indices)=mean(lnK);
else
    indices = false;
    inner_zone_nodes = false;
end

%% Load real data:
fid = fopen(obsfile);
s_obs_ref = textscan(fid, '%f%*[^\n]','HeaderLines',1);
fid = fclose(fid);
s_obs_ref = s_obs_ref{1};

    
%% Time discretization
dt = 60; %60;          % time increment [s]
tend = 86400; %86400;     % end time [s]
nt = tend/dt+1;   % number of time steps
store_step = 20;  % how often is c stored (every store_step-th time)
n_store=(nt-1)/store_step+1; % number of stored concentrations
%dt_store = dt*store_step;

if prior_assimtest
    load(ini_ens_file, 'lnKpost');     % Check if this does not override the already defined variables.
    lnKprior = lnKpost;
    clear lnKpost
elseif ~prior_assimtest
    load(fullfile(results_dir,'_ini_ensemble'))
end % prior_assimtest


meanlnKprior = mean(lnKprior,2);
stdlnKprior = std(lnKprior,[],2);

%% Initialize inversion:
if info.invert_inner_zone
    info.indices= indices;
    info.inner_zone_nodes = inner_zone_nodes;
end
if inversion_heads
    resfile = fullfile(results_dir_test, strcat(test_name,'_heads_',addstr));
    disp([datestr(clock) ': Assimilate hydraulic head data']);
    % add measurement error to obtain virtual measurements
    Cdd = spdiags(sigma_h^2*ones(nobs,1),0,nobs,nobs);
    %data_h = s_obs_ref+randn(length(s_obs_ref),1)*sigma_h;
    data_h = s_obs_ref;
    [lnKpost,h_sim,objfun,hall,done] = inversekernel(lnKprior,data_h,Cdd,type,...
                                                        nelflotra,info,@calculate_head3d,...
                                                        nyel, nxel, nzelflotra,nelflotra,...
                                                        dx,dy,dzflotra,...
                                                        Xflotra,Yflotra,Zflotra, hin, hout,... 
                                                        wellObj,AMGflag, plotflag);
    save(resfile, 'hin', 'hout', 'wellObj','Cdd','lnKpost', 'data_h' ,'h_sim','objfun','hall','done', 'sigma_h','info', '-v7.3')
    save(strcat(resfile,'_full.mat'),'-v7.3')
    disp([datestr(clock) ': Finished assimilating heads'])
elseif ~inversion_heads
    resfile = fullfile(results_dir_test, strcat(test_name,'_heads_',addstr));
    load(resfile)
end % inversion_heads


meanlnKpost = mean(lnKpost,2);
meanhall    = mean(hall,2);
stdlnKpost  = std(lnKpost,[],2);

if info.plotflag
    nx=length(dx);Lx=sum(dx);
    ny=length(dy);Ly=sum(dy);
    nz=length(dzflotra);Lz=sum(dzflotra);
    lnKr = reshape(meanlnKpost, length(dy),length(dx), length(dzflotra));

    figure(1)
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(2,2,1)
    h = slice(Xflotra,Yflotra,Zflotra,lnKr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
    set(h,'edgecolor','none')
    daspect([1 1 1]);
    colormap jet
    box on;
    colorbar('Location','southoutside')
    title('True Field of ln(K)')
    ca_lnKtrue=caxis;
    hold on
    tempobs = fieldnames(wellObj.nod_obs);
    tempwell =   fieldnames(wellObj.nod_well);

    for cur_field=1:numel(tempwell)
        cur_well_nodes = wellObj.nod_well.(tempwell{cur_field});
        scatter3(Xflotra(cur_well_nodes), Yflotra(cur_well_nodes), Zflotra(cur_well_nodes),7, 'k', 'filled');
    end         


    for cur_field=1:numel(tempobs)
        cur_obs_nodes = wellObj.nod_obs.(tempobs{cur_field});
        scatter3(Xflotra(cur_obs_nodes), Yflotra(cur_obs_nodes), Zflotra(cur_obs_nodes),5, 'k', 'filled');
    end         
    hold off

    meanlnKpriorr = reshape(meanlnKprior, length(dy),length(dx), length(dzflotra));
    figure(1)
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(2,2,2)
    h1 = slice(Xflotra,Yflotra,Zflotra,meanlnKpriorr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
    set(h1,'edgecolor','none')
    daspect([1 1 1]);
    colormap jet
    box on;
    colorbar('Location','southoutside')
    title('Prior Mean of ln(K)')
    caxis(ca_lnKtrue);

    meanlnKpostr = reshape(meanlnKpost, length(dy),length(dx), length(dzflotra));
    figure(1)
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(2,2,3)
    h2 = slice(Xflotra,Yflotra,Zflotra,meanlnKpostr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
    set(h2,'edgecolor','none')
    daspect([1 1 1]);
    colormap jet
    box on;
    colorbar('Location','southoutside')
    title('Posterior Mean of ln(K)')
    caxis(ca_lnKtrue);


    stdlnKriorr= reshape(stdlnKprior, length(dy),length(dx), length(dzflotra));
    figure(5)
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(1,3,1)
    h3 = slice(Xflotra,Yflotra,Zflotra,stdlnKriorr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
    set(h3,'edgecolor','none')
    daspect([1 1 1]);
    colormap jet
    box on;
    colorbar('Location','southoutside')
    title('Prior Standard Deviation of ln(K)')
    ca_stdprior=caxis;

    stdlnKpostr= reshape(stdlnKpost, length(dy),length(dx), length(dzflotra));
    figure(5)
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(1,3,2)
    h4 = slice(Xflotra,Yflotra,Zflotra,stdlnKpostr,[-.1]*Lx,[-.1]*Ly,[-.7]*Lz);
    set(h4,'edgecolor','none')
    daspect([1 1 1]);
    colormap jet
    box on;
    colorbar('Location','southoutside')
    title('Posterior Standard Deviation of ln(K): flow')
    caxis(ca_stdprior);

    figure(3)
    normres=(h_sim(:)-reshape(data_h*ones(1,size(h_sim,2)),nobs*size(h_sim,2),1))/sigma_h;
    handle=histfit(normres);
    set(handle(1),'facecolor',[.5 .5 .5],'edgecolor','none')
    xlabel('(h_{sim}-h_{meas})/\sigma_h [-]')
    ylabel('count')
    set(handle(2),'color','k')
    [fitmean,fitstd]=normfit(normres);
    legend('final ensemble',sprintf('N(%5.2f,%5.2f)',[fitmean,fitstd]))
end

%% Run model predictions
if run_predictions
    [nel,nreal_post] = size(lnKpost);
    disp([datestr(clock) ': Running model predictions']) 
    heads_pred = nan(nobs,nreal_post);
    heads_ini = nan(nobs,nreal);
    dwdn_pred= nan(nobs,nreal_post);
    dwdn_ini= nan(nobs,nreal);
    pool=parpool(pool_profile,nproc);
    parfor ireal=1:nreal
%             [heads_pred,Yall,~, dwdn_pred, sall]
        [heads_pred(:,ireal),~,~, dwdn_pred(:,ireal), ~] = calculate_head3d(lnKpost(:,ireal),...
                                             nyel, nxel, nzelflotra,nelflotra,...
                                             dx,dy,dzflotra,...
                                             Xflotra,Yflotra,Zflotra,...
                                             hin, hout,wellObj,... 
                                             AMGflag, plotflag);
    end
    poolobj = gcp('nocreate');
    delete(pool)

    pool=parpool(pool_profile,nproc);
    parfor ireal=1:nreal_post
%             [heads_pred,Yall,~, dwdn_pred, sall]
        [heads_ini(:,ireal),~,~, dwdn_ini(:,ireal), ~] = calculate_head3d(lnKprior(:,ireal),...
                                             nyel, nxel, nzelflotra,nelflotra,...
                                             dx,dy,dzflotra,...
                                             Xflotra,Yflotra,Zflotra,...
                                             hin, hout,wellObj,... 
                                             AMGflag, plotflag);
    end
    poolobj = gcp('nocreate');
    delete(pool)
    save(strcat(resfile,'pred'), 'heads_pred', 'heads_ini',...
                                  'dwdn_pred','dwdn_ini', '-v7.3')
    disp([datestr(clock) ': Finish model predictions'])
end
% h_mean = mean(dwdn_ini,2);
% h_std = std(dwdn_ini,1,2);
% lxline = linspace(-0.05, 0.2);
% errorbar(data_h,h_mean, h_std,'ok')
% hold on
% plot(lxline, lxline, '-b')

% h_mean = mean(dwdn_pred,2);
% h_std = std(dwdn_pred,1,2);
% lxline = linspace(-0.05, 0.2);
% errorbar(data_h,h_mean, h_std,'or')
% hold on
toc