tic
clear
close all
%% Generate initial ensemble for DA with the KEG.
% Generates a 3D reference scenario if wanted.
% Runs either steady-state flow alone or coupled with transport.
% THIS VERSION IS FOR THE ASSIMILATION OF REAL HT AND TT DATA

% Assimilation order for flow: 3b then 3a then 1b
% Assimilation order for transport: 3a then 2a then 3b
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 1:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make an initial run to evaluate and plot the initial state of the model?
make_inirun = false;
% Generate initial ensemble?
gen_ensemble = true;
% Limit parameter updating to the inner model area
invert_inner_zone = true;
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 2:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Number of realizations
nreal = 128*4;
% Random correlation lengths:
rand_corrs = true;
% Test name:  options: 3dtest_3b, 3dtest_3a, 3dtest_1b, 3dtest_3aTR then 3dtest_2aTR then 3dtest_3bTR
test_name = '3dtest_3b';
% Additional test identifier, will be attached to the files with the results:
addstr = '512m_03sigmah';
% Number of processors and pool profile
pool_profile = 'local';
nproc = 4;
% Store data?
storeData = true;
% Activate plot settings?
plotflag = false;

store_dir = 'results';
store_file = strcat('_ini_ensemble_test');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Settings section 3:
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% uncertainty of heads
sigma_h = 0.03; %0.01;
%% Boundary conditions
hin  = 0.01; %0.33;
hout = 0;
Ctype = 1;    % covariance model (1: exponential, 2: Gaussian)

%% Set path to AMG preconditioner and check whether the mex-file can be found
addpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
javaaddpath('/beegfs/work/emgsa01/software/hsl_mi20-2.0.0/matlab')
addpath('/home/olaf/hsl_mi20-2.0.0/matlab/')
javaaddpath('/home/olaf/hsl_mi20-2.0.0/matlab/')

mkdir(store_dir);
addpath(strcat('./', store_dir))

if exist('hsl_mi20','file')==3
   AMGflag = true;
   disp([datestr(clock) ': AMG preconditioner found']);
else
   AMGflag = false;
   disp([datestr(clock) ': No AMG preconditioner found, work with ILU']);
end

%% reset the global random number stream based on current time
s = RandStream('mt19937ar','Seed','shuffle');
RandStream.setGlobalStream(s);

%% Define the grid
zflotramin = -10.01;%-9.01;
zflotramax = -1.99; %-2.99;
disp([datestr(now) ': Creating model grid'])
% build the grid: coordinates of nodes
[dx,dy,dz,Xc,Yc,Zc,dzflotra,Xflotra,Yflotra,Zflotra]=...
    grid3D(zflotramin,zflotramax);
% number of elements in all directions
nyel=length(dy);
nxel=length(dx);
nzel=length(dz);
nzelflotra=length(dzflotra);
nel=nxel*nyel*nzel;
nelflotra=nxel*nyel*nzelflotra;

% mapper indices large domain -> flow-and-transport domain
z_map_flotra = findnodes(Xc(1,1,:),Yc(1,1,:),Zc(1,1,:),...
                         Xc(1)+[-.01 0.01],Yc(1)+[-0.01 0.01],...
                         [zflotramin zflotramax]);
% node numbers in 3-D array
elnum=reshape(1:nxel*nyel*nzel,nyel,nxel,nzel);
elnumflotra=reshape(1:nxel*nyel*nzelflotra,nyel,nxel,nzelflotra);

% cells at the in- and outflow faces
nodein  = elnum(:,1,:);nodein=nodein(:);
nodeout = elnum(:,nxel,:);nodeout=nodeout(:);
nodeinflotra  = elnumflotra(:,1,:);nodeinflotra=nodeinflotra(:);
nodeoutflotra = elnumflotra(:,nxel,:);nodeoutflotra=nodeoutflotra(:);

%% Define stuff specific for each test:
% Observation points and pumping wells
% Define the wells            
% well_1: outer injection well          well_4: inner injection well bottom
% well_2: inner injection well top      well_5: inner extraction well   
% well_3: inner injection well middle   well_6: outer extraction well     

%% Injection and extraction rates [m3/s]
% Test 3b:
if isequal(test_name,'3dtest_3b')
    wellObj.Q = struct('Qin1', 4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.82e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.0e-3, 'Qout2', -8.7e-3);
    well_file = './field_data/test_wells3b.txt';
    obsfile = './field_data/drdwn_3b.txt';
% Test 3a: 22062016
elseif isequal(test_name,'3dtest_3a')
    wellObj.Q = struct('Qin1', 5.2e-3, 'Qin2', 1.83e-3, 'Qin3', 0.9e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.5e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells3a.txt';
    obsfile = './field_data/drdwn_3a.txt';

% Test 1b: 
elseif isequal(test_name,'3dtest_1b')
    wellObj.Q = struct('Qin1',4.9e-3, 'Qin2', 2.1e-3, 'Qin3', 0.87e-3,...
                       'Qin4', 0.6e-3, 'Qout1', -2.4e-3, 'Qout2', -9.0e-3);
    well_file = './field_data/test_wells1b.txt';
    obsfile = './field_data/drdwn_1b.txt';
end

% nodes belonging to the injection and extraction screens
rangeXwells = [-18.01 -17.99; -8.01  -7.99; -8.01  -7.99; -8.01  -7.99; 7.99  8.01; 12.99 13.01];
rangeYwells = [-0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01; -0.01 0.01];
rangeZwells = [-9.01 -2.99; -4.51 -3.49;  -6.51 -5.49; -8.51 -7.99; -9.01 -2.99; -9.01 -2.99];
wellObj.nod_well = findnodes_many(Xflotra,Yflotra,Zflotra,...
                           rangeXwells,rangeYwells,rangeZwells);
% nodes belonging to the observation wells
% 1 -- 6 -- 11 -- 16
% |    |     |    |             xflat = Xflotra(:,:,1);
% 2 -- 7 -- 12 -- 17            yflat= Yflotra(:,:,1);
% |    |     |    |             plot(xflat(:), yflat(:), '.')
% 3 -- 8 -- 13 -- 18           
% |    |     |    |
% 4 -- 9 -- 14 -- 19
% |    |     |    |
% 5 -- 10 -- 15 -- 20
% rangeXwells_obs = [-5.01 -4.99;  -5.01 -4.99; -5.01 -4.99; -5.01 -4.99; -5.01 -4.99];
% rangeYwells_obs = [4.99 5.01;    1.6     1.7;  -0.01 0.01; -1.7 -1.6;  -5.01 -4.99];
% rangeZwells_obs = [-10.01 -1.99; -2.3 -2.1;  -10.01 -1.99; -10.01 -1.99;  -10.01 -1.99];
% arriv_obs_file = '';

% load observation well ranges from file
fid = fopen(well_file);
data = textscan(fid, '%f %f %f %f %f %f%*[^\n]','HeaderLines',1);
fid = fclose(fid);

rangeXwells_obs = [data{1} data{2}];
rangeYwells_obs = [data{3} data{4}];
rangeZwells_obs = [data{5} data{6}];

wellObj.nod_obs = findnodes_many(Xflotra,Yflotra,Zflotra,...
                            rangeXwells_obs, rangeYwells_obs, rangeZwells_obs);
                            
wellObj.nod_obs_c = wellObj.nod_obs;
% number of observations
fields = fieldnames(wellObj.nod_obs);
nobs = numel(fields);            
if invert_inner_zone
    inner_zone_nodes = findnodes_many(Xflotra,Yflotra,Zflotra, [-19.26, 15.26], [-9.26, 9.26],[-9.01 -2.99]);
end

%% Load real data:
fid = fopen(obsfile);
s_obs_ref = textscan(fid, '%f%*[^\n]','HeaderLines',1);
fid = fclose(fid);
s_obs_ref = s_obs_ref{1};

%% Generate hydraulic conductivity field
if ~rand_corrs
    lx = ones(nreal,1)*80;      % correlation length in number of cells x-direction
    ly = ones(nreal,1)*60;      % correlation length in number of cells y-direction
    lz = ones(nreal,1)*5;
    Kg  = 2.5e-7;
    sigma2 = 1.0;   % variance of log-conductivity
    
elseif rand_corrs
    minKgtop = 1.8e-3; % geometric mean
    maxKgtop = 2.4e-3; % geometric mean
    sigma2top = 0.23;   % variance of log-conductivity

    minKgbot = 1.2e-3; % geometric mean
    maxKgbot = 1.8e-3; % geometric mean
    sigma2bot = 3.8;   % variance of log-conductivity

    top_thickness = randi([21 30],nreal,1);
    bot_thickness = 80 - top_thickness;

    lxtop = (80-40).*rand(nreal,1) + 40;           % randi([75 100],nreal,1);
    lytop = lxtop;                                 % randi([50 75],nreal,1);
    lztop = (2.7-2).*rand(nreal,1) + 2;       % correlation length in number of cells z-direction
    thick_top = 1;
    
    lxbot = (18-10).*rand(nreal,1) + 40;           % randi([75 100],nreal,1);
    lybot = lxbot;                                 % randi([50 75],nreal,1);
    lzbot = (3.4-2.9).*rand(nreal,1) + 2;       % correlation length in number of cells z-direction    
    %lx = (110-75).*rand(nreal,1) + 75;
    %ly = (75-50).*rand(nreal,1) + 50;
end
    
if invert_inner_zone
    indices = zeros(nelflotra,1);
    indices(inner_zone_nodes) = 1;
%         lnK(~indices)=mean(lnK);
else
    indices = false;
    inner_zone_nodes = false;
end
    
if gen_ensemble
    % generate initial ensemble
    disp([datestr(now) ': Generate initial ensemble'])
    lnKprior = nan(nelflotra,nreal);
    % pool=parpool; % start parallel session
    pool=parpool(pool_profile,nproc);
    if rand_corrs
        parfor ireal=1:nreal
    %     for ireal=1:nreal
            Kgtop  = minKgtop*(rand(1)*log(maxKgtop/minKgtop));
            % Kg  = 2.5e-7;
            nowtop = k_generate(dx,dy,dz,lxtop(nreal),lytop(nreal),lztop(nreal),Ctype,Kgtop,sigma2top,false);
            K_nowtop = nowtop(:,:,z_map_flotra);

            Kgbot  = minKgbot*(rand(1)*log(maxKgbot/minKgbot));
            % Kg  = 2.5e-7;
            nowbot = k_generate(dx,dy,dz,lxbot(nreal),lybot(nreal),lzbot(nreal),Ctype,Kgbot,sigma2bot,false);
            K_nowbot = nowbot(:,:,z_map_flotra);

            K_all = K_nowtop;
            K_all(:,:,1:bot_thickness(ireal)) = K_nowbot(:,:,1:bot_thickness(ireal));

            lnKprior(:,ireal) = (K_all(:));
        end % parfor
    
    
    elseif ~rand_corrs
        parfor ireal=1:nreal        
            now = k_generate(dx,dy,dz,lx(nreal),ly(nreal),lz(nreal),Ctype,Kg,sigma2,false);
            K_now = now(:,:,z_map_flotra);
            lnKprior(:,ireal) = (K_now(:));
        end  % parfor
    end% rand_corss
    
    
    if invert_inner_zone
        lnKprior(~indices,:)=mean(lnKprior(:));
    end
        
    poolobj = gcp('nocreate');
    delete(pool)
   
end   % gen_ensemble

% Run a reference scenario:
if make_inirun
    disp([datestr(now) ': Making an initial model run with mean ensemble of parameters'])

    if ~gen_ensemble
        load(fullfile(store_dir, strcat(store_file,'.mat')));
    end % ~gen_ensemble
    
    meanlnKprior = mean(lnKprior,2);
    stdlnKprior = std(lnKprior,[],2);
    temp_plotflag = true;
    
    [Yobs,Yall,~, sobs, sall] = calculate_head3d(meanlnKprior,...
                                                 nyel, nxel, nzelflotra,nelflotra,...
                                                 dx,dy,dzflotra,...
                                                 Xflotra,Yflotra,Zflotra,...
                                                 hin, hout,wellObj,... 
                                                 AMGflag, temp_plotflag);
    Lx=sum(dx);
    Ly=sum(dy);
    Lz=sum(dzflotra);
    lnKr = reshape(meanlnKprior(:), length(dy),length(dx), length(dzflotra));
    
    figure(21)
    nzorg=length(dzflotra);
    nx=length(dx);
    ny=length(dy);
    nxorg=nx;
    nyorg=ny;
    set(gcf,'outerposition',get(0,'screensize'))
    subplot(1,2,1)
    [Xex,Yex,Zex]=meshgrid(cumsum([0 dx])-sum(dx)*.5,...
                           cumsum([0 dy])-sum(dy)*.5,...
                           cumsum([0 dzflotra])-sum(dzflotra));
    ranex=zeros(size(Xex));
    ranex(1:ny,1:nx,1:length(dzflotra))=lnKr;   
    ranex = ranex(1:length(dy)+1,1:length(dx)+1,1:nzorg+1);   
    slice(Xex,Yex,Zex,ranex,[-.25 .25]*Lx,[],[-.75 -.25]*Lz);
    hold on
    plot3(-.25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
    plot3( .25*[1 1 1 1 1]*Lx,[-.5 -.5 .5 .5 -.5]*Ly,[-1 0 0 -1 -1]*Lz,'k')
    plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.25*[1 1 1 1 1]*Lz,'k')
    plot3([-.5 -.5 .5 .5 -.5]*Lx,[-.5 .5 .5 -.5 -.5]*Ly,-.75*[1 1 1 1 1]*Lz,'k')
    plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
    plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.75*[1 1]*Lz,'k')
    plot3([-.25 -.25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
    plot3([ .25  .25]*Lx,[-.5 .5]*Ly,-.25*[1 1]*Lz,'k')
    hold off
    clear Xex Yex Zex ranex
    daspect([1 1 1]);
    colormap jet
    box on;
    set(gca,'xgrid','on','ygrid','on','zgrid','on','fontsize',12);
    set(gca,'clim',[min(meanlnKprior), max(meanlnKprior)])
%     set(gca,'clim',sqrt(sigma2)/log(10)*3*[-1 1]+log10(Kg));
    xlabel('x_1');ylabel('x_2');zlabel('x_3');
    axis tight
    shading flat;
    title('log-conductivity field')
    colorbar
    set(gcf,'paperunits','centimeters','paperposition',[1 1 20 10])
    drawnow;

    subplot(1,2,2); hold on;
    plot(s_obs_ref, sobs, 'og');
    daspect([1 1 1]);
    xlim([-0.05,0.3]);
    ylim([-0.05,0.3]);
    grid on;

end

save(fullfile(store_dir, store_file),'-v7.3')