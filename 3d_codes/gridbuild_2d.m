function [dx,dy,X_forK,Y_forK,Xc,Yc,nel]=gridbuild_2d(nx, ny)
%% Generates a 2D grid

% Returns:

dx = ones(nx,1)*.1;
dy = ones(ny,1)*.1;
% dx = ones(nx,1);
% dy = ones(ny,1);

xvec=[0;cumsum(dx(:))];
yvec=[0;cumsum(dy(:))];
[X_forK,Y_forK]=meshgrid(xvec,yvec);

xc = 0.5*(xvec(1:end-1)+xvec(2:end));
yc = 0.5*(yvec(1:end-1)+yvec(2:end));

[Xc,Yc]=meshgrid(xc,yc);
nel = numel(Xc);

clear xvec yvec xc yc

end